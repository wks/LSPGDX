// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP;

import static ru.m210projects.Build.Engine.*;

import ru.m210projects.LSP.Main.UserFlag;
import ru.m210projects.LSP.Types.DemoFile;
import ru.m210projects.LSP.Types.PlayerStruct;
import ru.m210projects.LSP.Types.SwingDoor;

public class Globals {
	
	public static final int WHITEPAL = 168;

	// Sprite statnum values

	public static final short INANIMATE = 0;
	public static final short CHASE = 1;
	public static final short GUARD = 2;
	public static final short SHOTSPARK = 3;
	/** A maybe unused statnum */
	public static final short STATNUM_UNUSED4 = 4;
	public static final short EXPLOSION = 5;
	public static final short PROJECTILE = 6;
	/** A maybe unused statnum */
	public static final short STATNUM_UNUSED7 = 7;

	/**
	 * When an enemy is hit, he will stay in the PAIN state for a brief time, during which he cannot fight back or move.
	 * <p>
	 * This is an extension to the seemingly reverse-engineered game code.  IIRC, in the DOS version of L7P, attacking
	 * an enemy can cause the enemy to bleed, giving a visual clue that the enemy is hit.  Without any
	 * reverse-engineering skills, I (wks) just implemented it myself.
	 */
	public static final short PAIN = 8;

	public static final short DYING = 20;
	public static final short ATTACK = 21;

	/** States which the player can attack. */
	public static final short[] TARGETABLE_STATES = {CHASE, GUARD, PAIN, ATTACK};

	//// picnums of the player's hands holding different weapons

	// Melee weapons
	/** Picnum of the hand of the sword. Zhan Zhao's weapon. */
	public static final short MELEE1_HAND = 1090;
	/** Picnum of the hand of the dao (Chinese falchion, a single-edged sword). Ouyang Chun's weapon. */
	public static final short MELEE2_HAND = 1100;
	/** Picnum of the hand of the ji (Chinese halberd). Lu Fang's weapon. */
	public static final short MELEE3_HAND = 1040;
	/** Picnum of the hand of the claw. Han Zhang's weapon. */
	public static final short MELEE4_HAND = 1060;
	/** Picnum of the hand of the mace. Xu Qing's weapon. */
	public static final short MELEE5_HAND = 1030;
	/** Picnum of the hand of the dagger. Jiang Ping's weapon. */
	public static final short MELEE6_HAND = 1070;
	/** Picnum of the hand of the fan. Bai Yutang's weapon. */
	public static final short MELEE7_HAND = 1080;
	/** Picnum of the hand of the axe. The time-travelling player's own weapon. */
	public static final short MELEE8_HAND = 1050;

	// Anqi (consealed weapons, i.e. throwing weapons)

	/** Picnum of the hand of the 1st Anqi. Ancient Chinese coin. */
	public static final short ANQI1_HAND = 1160;

	/** Picnum of the hand of the 2nd Anqi. Iron ball. */
	public static final short ANQI2_HAND = 1150;

	/** Picnum of the hand of the 3rd Anqi. Needles. */
	public static final short ANQI3_HAND = 1140;

	/** Picnum of the hand of the 4th Anqi. Cross-shaped dart. */
	public static final short ANQI4_HAND = 1130;

	/** Picnum of the hand of the 5th Anqi. Dagger. */
	public static final short ANQI5_HAND = 1120;

	/** Picnum of the hand of the 6th Anqi. Throwing dart, like Kunai. */
	public static final short ANQI6_HAND = 1110;

	// Qigong (Qi techniques, i.e. fireballs)

	/** Picnum of the hand of the 1st Qigong. */
	public static final short QIGONG1_HAND = 1170;

	/** Picnum of the hand of the 2nd Qigong. */
	public static final short QIGONG2_HAND = 1183;

	/** Picnum of the hand of the 3rd Qigong. */
	public static final short QIGONG3_HAND = 1194;

	/** Picnum of the hand of the 4th Qigong. */
	public static final short QIGONG4_HAND = 1205;

	/** Picnum of the hand of the 5th Qigong. */
	public static final short QIGONG5_HAND = 1216;

	/** Picnum of the hand of the 6th Qigong. */
	public static final short QIGONG6_HAND = 1244;

	//// Picnums of player projectiles

	// Picnums of Anqi (concealed weapons, i.e. throwing weapons)

	/** Picnum of the projectile of the 1st Anqi. Ancient Chinese coin. */
	public static final short ANQI1_PROJ = 1166;

	/** Picnum of the projectile of the 2nd Anqi. Iron ball. */
	public static final short ANQI2_PROJ = 1156;

	/** Picnum of the projectile of the 3rd Anqi. Needles. */
	public static final short ANQI3_PROJ = 1146;

	/** Picnum of the projectile of the 4th Anqi. Cross-shaped dart. */
	public static final short ANQI4_PROJ = 1136;

	/** Picnum of the projectile of the 5th Anqi. Dagger. */
	public static final short ANQI5_PROJ = 1126;

	/** Picnum of the projectile of the 6th Anqi. Throwing dart, like Kunai. */
	public static final short ANQI6_PROJ = 1116;

	// Picnums of Qigong (Qi techniques, i.e. fireballs)

	/** Picnum of the projectile of the 1st Qigong. Red spinning fireball. Buddha statue also shoots this. */
	public static final short QIGONG1_PROJ = 1179;

	/** Picnum of the projectile of the 2nd Qigong. Orange fireball. */
	public static final short QIGONG2_PROJ = 1192;

	/** Picnum of the projectile of the 3rd Qigong. Green fireball. */
	public static final short QIGONG3_PROJ = 1203;

	/** Picnum of the projectile of the 4th Qigong. Lightning ball. */
	public static final short QIGONG4_PROJ = 1214;

	/** Picnum of the projectile of the 5th Qigong. Orange vertically-spinning fireballs. */
	public static final short QIGONG5_PROJ = 1225;

	/** Picnum of the projectile of the 6th Qigong. Green boomerang. */
	public static final short QIGONG6_PROJ = 1259;

	// picnums of enemies

	/** Buddha statue, shooting fireballs around. */
	public static final short BUDDHA = 232;

	/** Skull trap, shooting fireballs at player. */
	public static final short SKULL = 1410;

	/**
	 * Blue enemy, carrying dao (Chinese falchion).
	 * <p>
	 * Official name: Rank 4 royal guard.
	 * (Seriously? I think the enemies are over-ranked.
	 * Our protagonists Zhan Zhao (using sword) and Bai Yutang (using fan) are also Rank 4 royal guards,
	 * but they are much stronger than those weak blue dudes.)
	 */
	public static final short BLUEDUDE = 1536;

	/** Green enemy, carrying whip. Official name: Rank 3 royal guard. */
	public static final short GREENDUDE = 1792;

	/** Red enemy, attacking with Anqi (concealed weapon). Official name: Rank 2 royal guard. */
	public static final short REDDUDE = 2048;

	/**
	 * Enemy wearing wushamao (government official hat) and blue/purple-ish robe, attacking with Qigong.
	 * <p>
	 * Official name: Rank 1 government official.
	 * (Seriously? The highest-rank government officials are collaborating with the evil?
	 * Well, the government is certainly very, very corrupted.)
	 */
	public static final short PURPLEDUDE = 2304;

	/** Yellow warrior, attacking with dragon-shaped Qigong. Official name: Lulin (rogue) warrior. */
	public static final short YELLOWDUDE = 2560;

	/** An array of all dudes. */
	public static final short[] DUDES = {BLUEDUDE, GREENDUDE, REDDUDE, PURPLEDUDE, YELLOWDUDE};

	/** Offset from the first picnum of a dude to the picnum of his projectile */
	public static final short DUDE_TO_PROJECTILE = 48;

	// Projectiles from dudes

	/** Picnum of the projectile of the green dude. Green Qi from his whip. */
	public static final short GREENDUDE_PROJ = GREENDUDE + DUDE_TO_PROJECTILE; // 1840

	/** Picnum of the projectile of the red dude. Grey coins, like the player's ancient coin Anqi. */
	public static final short REDDUDE_PROJ = REDDUDE + DUDE_TO_PROJECTILE; // 2096

	/** Picnum of the projectile of the purple dude. Like the fireball of the player's first Qigong. */
	public static final short PURPLEDUDE_PROJ = PURPLEDUDE + DUDE_TO_PROJECTILE; // 2352

	/** Picnum of the projectile of the yellow dude. Dragon-shaped Qigong blast. */
	public static final short YELLOWDUDE_PROJ = YELLOWDUDE + DUDE_TO_PROJECTILE; // 2608

	/** An array of all projectiles from dudes. */
	public static final short[] DUDE_PROJS = {GREENDUDE_PROJ, REDDUDE_PROJ, PURPLEDUDE_PROJ, YELLOWDUDE_PROJ};

	// Projectiles from other enemies
	/** Picnum of the projectile from skull statue. An orange fireball. */
	public static final short SKULL_PROJ = 1322;

	// Other picnum constants;
	/** The first breakable sprites */
	public static final short BREAKABLE_BEGIN = 256;
	/** One after last breakable sprites */
	public static final short BREAKABLE_END = 320;
	/** Distance from the picnum of a breakable sprite to the picnum of its broken form. */
	public static final int BREAKABLE_TO_BROKEN = 64;

	public static final short PLAYER = 197; //249
	public static final short TILE_ANIM = (short) (MAXTILES - 2);
	public static final short ANIM_PAL = (short) (NORMALPAL - 1);
	
	public static final short MAXHEALTH = 160;
	public static final short MAXMANNA = 160;
	public static final short MAXARMOR = 160;
	public static final short MAXAMMO = 160;

	public static short nNextMap;
	public static boolean followmode = false;
	public static int followx, followy;
	public static int followvel, followsvel, followa;
	public static float followang;
	public static UserFlag mUserFlag = UserFlag.None;
	public static int nKickSprite;
	public static int nKickClock;
	public static int nMusicClock;
	
	public static final class MapStruct {
		public final byte num;
		public final byte nextmap[] = new byte[3];
		public final byte music;

		public MapStruct(int num, int map1, int map2, int map3, int music) {
			this.num = (byte) num;
			this.nextmap[0] = (byte) map1;
			this.nextmap[1] = (byte) map2;
			this.nextmap[2] = (byte) map3;
			this.music = (byte) music;
		}
	}

	public static final MapStruct maps[] = { 
			new MapStruct(111, 1, 1, 1, 4), 
			new MapStruct(111, 2, 3, 0, 9),
			new MapStruct(121, 3, 4, 5, 11), 
			new MapStruct(131, 4, 5, 0, 9), 
			new MapStruct(211, 5, 6, 7, 1),
			new MapStruct(221, 6, 7, 0, 12), 
			new MapStruct(231, 7, 8, 0, 11), 
			new MapStruct(112, 8, 0, 0, 13),
			new MapStruct(122, 9, 10, 0, 16), 
			new MapStruct(132, 10, 0, 0, 13), 
			new MapStruct(142, 11, 0, 0, 1),
			new MapStruct(152, 12, 0, 0, 16), 
			new MapStruct(113, 13, 0, 0, 6), 
			new MapStruct(123, 14, 0, 0, 17),
			new MapStruct(133, 15, 16, 0, 6), 
			new MapStruct(143, 16, 17, 0, 17), 
			new MapStruct(153, 17, 0, 0, 6),
			new MapStruct(114, 18, 0, 0, 10), 
			new MapStruct(214, 19, 0, 0, 14), 
			new MapStruct(115, 20, 21, 0, 1),
			new MapStruct(125, 22, 0, 0, 9), 
			new MapStruct(135, 22, 0, 0, 1), 
			new MapStruct(145, 23, 0, 0, 9),
			new MapStruct(155, 24, 0, 0, 6), 
			new MapStruct(165, 25, 0, 0, 9), 
			new MapStruct(116, 26, 27, 0, 7),
			new MapStruct(126, 27, 28, 0, 11), 
			new MapStruct(136, 29, 0, 0, 7), 
			new MapStruct(146, 29, 30, 0, 17),
			new MapStruct(216, 30, 31, 0, 6), 
			new MapStruct(226, 31, 32, 0, 7), 
			new MapStruct(236, 32, 0, 0, 11),
			new MapStruct(246, 33, 0, 0, 6), 
			new MapStruct(117, 99, 99, 99, 4), 
			new MapStruct(117, 99, 99, 99, 4), };
	
	
	public static PlayerStruct gPlayer[] = new PlayerStruct[MAXPLAYERS];
	public static short nDiffDoor, nDiffDoorBack, nTrainWall;
	public static boolean bActiveTrain;
	public static boolean bTrainSoundSwitch;
	public static final int TICSPERFRAME = 4;
	public static int lockclock;
	public static int totalmoves;
	public static int screenpeek = 0;
	public static int mapnum = -1;
	public static short nDifficult = 1;
	public static short nPlayerFirstWeapon = 17;
	public static int oldchoose;
	public static short oldpic;
	
	public static int book;
	public static int chapter;
	public static int verse;
	
	public static int recstat, m_recstat;
	public static DemoFile rec;
	public static final int RECSYNCBUFSIZ = 2520;

	public static short waterfountainwall[] = new short[MAXPLAYERS], waterfountaincnt[] = new short[MAXPLAYERS];

	public static short ypanningwallcnt;
	public static short[] ypanningwalllist = new short[16];

	public static short floorpanningcnt;
	public static short[] floorpanninglist = new short[16];

	public static short warpsectorcnt;
	public static short[] warpsectorlist = new short[16];

	public static short xpanningsectorcnt;
	public static short[] xpanningsectorlist = new short[16];

	public static short warpsector2cnt;
	public static short[] warpsector2list = new short[32];

	public static short subwaytracksector[][] = new short[5][128], subwaynumsectors[] = new short[5], subwaytrackcnt;
	public static int[] subwaystop[] = new int[5][8], subwaystopcnt = new int[5];
	public static int[] subwaytrackx1 = new int[5], subwaytracky1 = new int[5];
	public static int[] subwaytrackx2 = new int[5], subwaytracky2 = new int[5];
	public static int[] subwayx = new int[5], subwaygoalstop = new int[5], subwayvel = new int[5],
			subwaypausetime = new int[5];

	public static short revolvesector[] = new short[4], revolveang[] = new short[4], revolvecnt;
	public static int[][] revolvex = new int[4][48], revolvey = new int[4][48];
	public static int[] revolvepivotx = new int[4], revolvepivoty = new int[4];
	
	public static short swingcnt;
	public static final int   MAXSWINGDOORS = 32;
	public static SwingDoor[] swingdoor = new SwingDoor[MAXSWINGDOORS];

	public static short dragsectorlist[] = new short[16], dragxdir[] = new short[16], dragydir[] = new short[16],
			dragsectorcnt;
	public static int[] dragx1 = new int[16], dragy1 = new int[16], dragx2 = new int[16], dragy2 = new int[16],
			dragfloorz = new int[16];

	public static boolean showSectorInfo = false;
	public static boolean showFullMap = false;
	public static boolean showSprites = false;
}
