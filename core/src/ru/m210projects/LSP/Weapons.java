// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.LSP.Enemies.*;
import static ru.m210projects.LSP.Sprites.*;
import static ru.m210projects.LSP.Sounds.*;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.Main.*;
import static ru.m210projects.LSP.Player.*;

import ru.m210projects.Build.Types.SPRITE;

import java.util.Optional;

public class Weapons {

	/** Max number of times the metal ball bounces. */
	public static final short ANQI2_MAX_BOUNCE = 13;

	/** Lightning ball like in the classic game. */
	public static final short QIGONG4_HITAG_CLASSIC = 0;
	/** Big lightning ball in the revised game. */
	public static final short QIGONG4_HITAG_BIG = 1;
	/** Small lightning ball in the revised game. */
	public static final short QIGONG4_HITAG_SMALL = 2;

	/**
	 * Bouncing power of the "boomerang" of Qigong #6 bounces.
	 * It is reduced by 3 when hitting an enemy,
	 * and reduced by 1 when hitting a wall or other sprite.
	 */
	public static final short QIGONG6_BOUNCING_POWER = 30;
	/** The angle (as cosine) for searching target when launching Qigong #6 */
	public static final double QIGONG6_AIM_COS = Math.cos(Math.toRadians(30));

	private static int weapx, weapy;

	private static final short nWeaponTile[] = { 0,
			ANQI1_HAND, ANQI2_HAND, ANQI3_HAND, ANQI4_HAND, ANQI5_HAND, ANQI6_HAND,
			QIGONG1_HAND, QIGONG2_HAND, QIGONG3_HAND, QIGONG4_HAND, QIGONG5_HAND, QIGONG6_HAND,
			MELEE1_HAND, MELEE1_HAND, MELEE1_HAND, MELEE1_HAND,
			MELEE1_HAND, MELEE2_HAND, MELEE3_HAND, MELEE4_HAND, MELEE5_HAND, MELEE6_HAND, MELEE7_HAND, MELEE8_HAND,
			0, 0 };

	public static void overwritesprite(int thex, int they, int tilenum, int ang, int dapalnum, int stat) {
		engine.rotatesprite(thex << 16, they << 16, 65536, ang, tilenum, 0, dapalnum, stat, 0, 0, xdim, ydim);
	}

	public static int getIcon(int nWeapon) {
		int picnum = 660;
		if (nWeapon > 0 && nWeapon < 13)
			picnum += nWeapon;

		return nWeapon != 0 ? picnum : 0;
	}

	/**
	 * Map qigong and nWeaponSeq to value of nWeaponImpact.
	 */
	public static final int QIGONG_IMPACT_DATA[][] = {
		{-20, -24, -36,  20,  25,  43,  50}, // qigong 1
		{-20, -24, -36,  40,  20,  64,  32}, // qigong 2
		{-30, -32, -12, -20,  10,  24,  35}, // qigong 3
		{-42, -24, -12,  28,  24,  24,  24}, // qigong 4
		{-36, -12, -24, -24, -12, -28,  30, 64}, // qigong 5
		{-28, -20, -48, -10, -18, -11,   0,  0, 0, 0, 0, 64, 64, 64, 64} // qigong6
	};

	/**
	 * Some weapons (qigong) pushes the player backwards and forwards when firing.
	 * Apply the impact to the player.
	 */
	public static void applyWeaponImpact(int snum) {
		int curr = gPlayer[snum].nWeapon;
		int index = switch(nWeaponTile[curr]) {
			case QIGONG1_HAND -> 0;
			case QIGONG2_HAND -> 1;
			case QIGONG3_HAND -> 2;
			case QIGONG4_HAND -> 3;
			case QIGONG5_HAND -> 4;
			case QIGONG6_HAND -> 5;
			default -> -1;
		};
		if (index == -1) {
			return;
		}
		var curWeaponData = QIGONG_IMPACT_DATA[index];
		int nWeaponSeq = gPlayer[snum].nWeaponSeq;
		if (nWeaponSeq >= curWeaponData.length) {
			return;
		}
		gPlayer[snum].nWeaponImpact = (byte) curWeaponData[nWeaponSeq];
		gPlayer[snum].nWeaponImpactAngle = (short) gPlayer[snum].ang;

	}

	public static void moveweapons(int snum) {
		int currTime;
		int curr = gPlayer[snum].nWeapon;
		if (gPlayer[snum].nWeaponState != 0) {
			applyWeaponImpact(snum);
		}

		if (gPlayer[snum].nWeaponState == 0) {
			if (gPlayer[snum].nWeaponShootCount == 9 && gPlayer[snum].isWeaponFire != 0) {
				if (nWeaponTile[gPlayer[snum].nWeapon] >= MELEE7_HAND) { // Fan, sword and falchion.
					shoot(snum, gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, (int) gPlayer[snum].ang,
							(int) gPlayer[snum].horiz, gPlayer[snum].sectnum, gPlayer[snum].nWeapon);
				} else {
					shoot(snum, gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, (int) gPlayer[snum].ang,
							(int) gPlayer[snum].horiz, gPlayer[snum].sectnum, 0);
				}
				gPlayer[snum].nWeaponShootCount++;
			}
		} else {
			switch (nWeaponTile[gPlayer[snum].nWeapon]) {
			case MELEE2_HAND: // nWeapon 18
			case MELEE3_HAND: // nWeapon 19
				currTime = lockclock - gPlayer[snum].nWeaponClock;
				if (gPlayer[snum].nWeaponShootCount == 2) {
					shoot(snum, gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, (int) gPlayer[snum].ang,
							(int) gPlayer[snum].horiz, gPlayer[snum].sectnum, 0);
					gPlayer[snum].isWeaponFire = 0;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 88) {
					gPlayer[snum].nWeaponSeq = 0;
					gPlayer[snum].nWeaponState = 0;
					gPlayer[snum].nWeaponShootCount = 9;
					return;
				}

				if (currTime > 70) {
					gPlayer[snum].nWeaponSeq = 5;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 56) {
					gPlayer[snum].nWeaponSeq = 4;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 48) {
					gPlayer[snum].nWeaponSeq = 3;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 32) {
					gPlayer[snum].nWeaponSeq = 2;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 10) {
					gPlayer[snum].nWeaponSeq = 1;
					return;
				}

				gPlayer[snum].nWeaponSeq = 0;
				return;
			case MELEE1_HAND: // nWeapon 13 - 17
				currTime = lockclock - gPlayer[snum].nWeaponClock;
				if (gPlayer[snum].nWeaponShootCount == 2) {
					shoot(snum, gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, (int) gPlayer[snum].ang,
							(int) gPlayer[snum].horiz, gPlayer[snum].sectnum, 0);
					gPlayer[snum].isWeaponFire = 0;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 100) {
					gPlayer[snum].nWeaponSeq = 0;
					gPlayer[snum].nWeaponState = 0;
					gPlayer[snum].nWeaponShootCount = 9;
					return;
				}

				if (currTime > 88) {
					gPlayer[snum].nWeaponSeq = 6;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 70) {
					gPlayer[snum].nWeaponSeq = 5;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}
				if (currTime > 56) {
					gPlayer[snum].nWeaponSeq = 4;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}
				if (currTime > 48) {
					gPlayer[snum].nWeaponSeq = 3;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}
				if (currTime > 32) {
					gPlayer[snum].nWeaponSeq = 2;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}
				if (currTime > 10) {
					gPlayer[snum].nWeaponSeq = 1;
					return;
				}

				gPlayer[snum].nWeaponSeq = 0;
				return;

			case ANQI6_HAND: // nWeapon 6
			case ANQI5_HAND: // nWeapon 5
			case ANQI4_HAND: // nWeapon 4
			case ANQI3_HAND: // nWeapon 3
			case ANQI2_HAND: // nWeapon 2
			case ANQI1_HAND: // nWeapon 1
				currTime = lockclock - gPlayer[snum].nWeaponClock;

				// Allow each attack to shoot multiple projectiles.
				boolean shouldShoot = gPlayer[snum].nWeaponShootCount == 0;
				boolean lastShot = true;

				if (cfg.bRevisedAnqi3 && nWeaponTile[gPlayer[snum].nWeapon] == 1140) {
					switch (gPlayer[snum].nWeaponShootCount) {
						case -8:
							lastShot = true;
							// FALLTHROUGH
						case -4:
							shouldShoot = true;
							break;
						default:
							break;
					}
				}

				if (currTime > 108) {
					gPlayer[snum].nWeaponSeq = 0;
					gPlayer[snum].nWeaponState = 0;
					gPlayer[snum].nWeaponShootCount = 9;
				} else if (currTime > 72) {
					gPlayer[snum].nWeaponSeq = 5;
					gPlayer[snum].nWeaponShootCount--;
				} else if (currTime > 60) {
					gPlayer[snum].nWeaponSeq = 4;
					gPlayer[snum].nWeaponShootCount--;
				} else if (currTime > 48) {
					gPlayer[snum].nWeaponSeq = 3;
					gPlayer[snum].nWeaponShootCount--;
				} else if (currTime > 32) {
					gPlayer[snum].nWeaponSeq = 2;
					gPlayer[snum].nWeaponShootCount--;
				} else if (currTime > 16) {
					gPlayer[snum].nWeaponSeq = 1;
				} else {
					gPlayer[snum].nWeaponSeq = 0;
				}

				if (shouldShoot) {
					shoot(snum, gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, (int) gPlayer[snum].ang,
							(int) gPlayer[snum].horiz, gPlayer[snum].sectnum, gPlayer[snum].nWeapon);
					if (lastShot) {
						gPlayer[snum].isWeaponFire = 0;
					}
				}
				return;
			case MELEE6_HAND: // nWeapon 22
				currTime = lockclock - gPlayer[snum].nWeaponClock;
				if (gPlayer[snum].nWeaponShootCount == 2) {
					shoot(snum, gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, (int) gPlayer[snum].ang,
							(int) gPlayer[snum].horiz, gPlayer[snum].sectnum, 0);
					gPlayer[snum].isWeaponFire = 0;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 68) {
					gPlayer[snum].nWeaponSeq = 0;
					gPlayer[snum].nWeaponState = 0;
					gPlayer[snum].nWeaponShootCount = 9;
					return;
				}
				if (currTime > 48) {
					gPlayer[snum].nWeaponSeq = 3;
					return;
				}
				if (currTime > 32) {
					gPlayer[snum].nWeaponSeq = 2;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}
				if (currTime > 10) {
					gPlayer[snum].nWeaponSeq = 1;
					return;
				}
				gPlayer[snum].nWeaponSeq = 0;
				return;
			case MELEE7_HAND: // nWeapon 23
				currTime = lockclock - gPlayer[snum].nWeaponClock;
				if (gPlayer[snum].nWeaponShootCount == 2) {
					shoot(snum, gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, (int) gPlayer[snum].ang,
							(int) gPlayer[snum].horiz, gPlayer[snum].sectnum, 0);
					gPlayer[snum].isWeaponFire = 0;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 88) {
					gPlayer[snum].nWeaponSeq = 0;
					gPlayer[snum].nWeaponState = 0;
					gPlayer[snum].nWeaponShootCount = 3;
					return;
				}

				if (currTime > 60) {
					gPlayer[snum].nWeaponSeq = 4;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 48) {
					gPlayer[snum].nWeaponSeq = 3;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 32) {
					gPlayer[snum].nWeaponSeq = 2;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 10) {
					gPlayer[snum].nWeaponSeq = 1;
					return;
				}

				gPlayer[snum].nWeaponSeq = 0;
				return;
			case MELEE5_HAND: // nWeapon 21
				currTime = lockclock - gPlayer[snum].nWeaponClock;
				if (gPlayer[snum].nWeaponShootCount == 2) {
					shoot(snum, gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, (int) gPlayer[snum].ang,
							(int) gPlayer[snum].horiz, gPlayer[snum].sectnum, 0);
					gPlayer[snum].isWeaponFire = 0;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 88) {
					gPlayer[snum].nWeaponSeq = 0;
					gPlayer[snum].nWeaponState = 0;
					gPlayer[snum].nWeaponShootCount = 3;
					return;
				}

				if (currTime > 70) {
					gPlayer[snum].nWeaponSeq = 5;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 56) {
					gPlayer[snum].nWeaponSeq = 4;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 48) {
					gPlayer[snum].nWeaponSeq = 3;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 32) {
					gPlayer[snum].nWeaponSeq = 2;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 10) {
					gPlayer[snum].nWeaponSeq = 1;
					return;
				}

				gPlayer[snum].nWeaponSeq = 0;
				return;
			case MELEE8_HAND: // nWeapon 24
				currTime = lockclock - gPlayer[snum].nWeaponClock;
				if (gPlayer[snum].nWeaponShootCount == 13) {
					shoot(snum, gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, (int) gPlayer[snum].ang,
							(int) gPlayer[snum].horiz, gPlayer[snum].sectnum, 0);
					gPlayer[snum].isWeaponFire = 0;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 100) {
					gPlayer[snum].nWeaponSeq = 0;
					gPlayer[snum].nWeaponState = 0;
					gPlayer[snum].nWeaponShootCount = 9;
					return;
				}

				if (currTime > 88) {
					gPlayer[snum].nWeaponSeq = 5;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 60) {
					gPlayer[snum].nWeaponSeq = 4;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 48) {
					gPlayer[snum].nWeaponSeq = 3;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 32) {
					gPlayer[snum].nWeaponSeq = 2;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 10) {
					gPlayer[snum].nWeaponSeq = 1;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}
				gPlayer[snum].nWeaponSeq = 0;
				return;
			case MELEE4_HAND: // nWeapon 20
				currTime = lockclock - gPlayer[snum].nWeaponClock;
				if (gPlayer[snum].nWeaponShootCount == 2) {
					shoot(snum, gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, (int) gPlayer[snum].ang,
							(int) gPlayer[snum].horiz, gPlayer[snum].sectnum, 0);
					gPlayer[snum].isWeaponFire = 0;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 88) {
					gPlayer[snum].nWeaponSeq = 0;
					gPlayer[snum].nWeaponState = 0;
					gPlayer[snum].nWeaponShootCount = 3;
					return;
				}

				if (currTime > 66) {
					gPlayer[snum].nWeaponSeq = 4;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 48) {
					gPlayer[snum].nWeaponSeq = 3;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 32) {
					gPlayer[snum].nWeaponSeq = 2;
					gPlayer[snum].nWeaponShootCount++;
					return;
				}

				if (currTime > 10) {
					gPlayer[snum].nWeaponSeq = 1;
					return;
				}
				gPlayer[snum].nWeaponSeq = 0;
				return;

			case QIGONG1_HAND: // nWeapon 7
			case QIGONG2_HAND: // nWeapon 8
			case QIGONG3_HAND: // nWeapon 9
			case QIGONG4_HAND: // nWeapon 10
				currTime = lockclock - gPlayer[snum].nWeaponClock;
				if (gPlayer[snum].nWeaponShootCount != 0) {
					if (currTime > 134) {
						gPlayer[snum].nWeaponSeq = 0;
						gPlayer[snum].nWeaponState = 0;
						gPlayer[snum].nWeaponShootCount = 9;
						return;
					}
					if (currTime > 110) {
						gPlayer[snum].nWeaponSeq = 6;
						gPlayer[snum].nWeaponShootCount--;
						return;
					}
					if (currTime > 94) {
						gPlayer[snum].nWeaponSeq = 5;
						gPlayer[snum].nWeaponShootCount--;
						return;
					}
					if (currTime > 82) {
						gPlayer[snum].nWeaponSeq = 4;
						return;
					}
					if (currTime > 70) {
						gPlayer[snum].nWeaponSeq = 3;
						return;
					}
					if (currTime > 32) {
						gPlayer[snum].nWeaponSeq = 2;
						return;
					}
					gPlayer[snum].nWeaponSeq = 0;
					return;
				}
				shoot(snum, gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, (int) gPlayer[snum].ang,
						(int) gPlayer[snum].horiz, gPlayer[snum].sectnum, gPlayer[snum].nWeapon);
				gPlayer[snum].isWeaponFire = 0;
				gPlayer[snum].nWeaponShootCount--;
				return;
			case QIGONG6_HAND: // nWeapon 12
				currTime = lockclock - gPlayer[snum].nWeaponClock;
				if (gPlayer[snum].nWeaponShootCount != 0) {

					if (currTime > 240) {
						gPlayer[snum].nWeaponSeq = 0;
						gPlayer[snum].nWeaponState = 0;
						gPlayer[snum].nWeaponShootCount = 9;
						return;
					}

					if (currTime > 228) {
						gPlayer[snum].nWeaponSeq = 15;
						gPlayer[snum].nWeaponShootCount--;
						return;
					}
					if (currTime > 208) {
						gPlayer[snum].nWeaponSeq = 14;
						gPlayer[snum].nWeaponShootCount--;
						return;
					}
					if (currTime > 180) {
						gPlayer[snum].nWeaponSeq = 13;
						return;
					}
					if (currTime > 160) {
						gPlayer[snum].nWeaponSeq = 12;
						return;
					}
					if (currTime > 144) {
						gPlayer[snum].nWeaponSeq = 11;
						return;
					}
					if (currTime > 126) {
						gPlayer[snum].nWeaponSeq = 10;
						return;
					}
					if (currTime > 116) {
						gPlayer[snum].nWeaponSeq = 9;
						return;
					}
					if (currTime > 106) {
						gPlayer[snum].nWeaponSeq = 8;
						return;
					}
					if (currTime > 96) {
						gPlayer[snum].nWeaponSeq = 7;
						return;
					}
					if (currTime > 72) {
						gPlayer[snum].nWeaponSeq = 6;
						return;
					}
					if (currTime > 64) {
						gPlayer[snum].nWeaponSeq = 5;
						return;
					}
					if (currTime > 50) {
						gPlayer[snum].nWeaponSeq = 4;
						return;
					}
					if (currTime > 36) {
						gPlayer[snum].nWeaponSeq = 3;
						return;
					}
					if (currTime > 24) {
						gPlayer[snum].nWeaponSeq = 2;
						return;
					}
					if (currTime > 12) {
						gPlayer[snum].nWeaponSeq = 1;
						return;
					}

					gPlayer[snum].nWeaponSeq = 0;
					return;
				}

				shoot(snum, gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, (int) gPlayer[snum].ang,
						(int) gPlayer[snum].horiz, gPlayer[snum].sectnum, gPlayer[snum].nWeapon);
				gPlayer[snum].isWeaponFire = 0;
				gPlayer[snum].nWeaponShootCount--;
				return;
			case QIGONG5_HAND: // nWeapon 11
				currTime = lockclock - gPlayer[snum].nWeaponClock;
				if (gPlayer[snum].nWeaponShootCount == 0) {
					shoot(snum, gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, (int) gPlayer[snum].ang,
							(int) gPlayer[snum].horiz, gPlayer[snum].sectnum, gPlayer[snum].nWeapon);
					gPlayer[snum].isWeaponFire = 0;
					gPlayer[snum].nWeaponShootCount--;
					return;
				}
				if (currTime > 720) {
					gPlayer[snum].nWeaponSeq = 0;
					gPlayer[snum].nWeaponState = 0;
					gPlayer[snum].nWeaponShootCount = 9;
					return;
				}
				if (currTime > 700) {
					gPlayer[snum].nWeaponSeq = 1;
					return;
				}
				if (currTime > 685) {
					gPlayer[snum].nWeaponSeq = 2;
					return;
				}
				if (currTime > 660) {
					gPlayer[snum].nWeaponSeq = 3;
					return;
				}
				if (currTime > 638) {
					gPlayer[snum].nWeaponSeq = 4;
					return;
				}
				if (currTime > 628) {
					gPlayer[snum].nWeaponSeq = 5;
					return;
				}
				if (currTime > 600) {
					gPlayer[snum].nWeaponSeq = 6;
					return;
				}
				if (currTime > 580) {
					gPlayer[snum].nWeaponSeq = 5;
					if (gPlayer[snum].nWeaponShootCount != 1)
						return;
					gPlayer[snum].nWeaponShootCount--;
					return;
				}
				if (currTime > 550) {
					gPlayer[snum].nWeaponSeq = 6;
					gPlayer[snum].nWeaponShootCount = 1;
					return;
				}
				if (currTime > 520) {
					gPlayer[snum].nWeaponSeq = 5;
					if (gPlayer[snum].nWeaponShootCount != 1)
						return;
					gPlayer[snum].nWeaponShootCount--;
					return;
				}
				if (currTime > 490) {
					gPlayer[snum].nWeaponSeq = 6;
					gPlayer[snum].nWeaponShootCount = 1;
					return;
				}
				if (currTime > 460) {
					gPlayer[snum].nWeaponSeq = 5;
					if (gPlayer[snum].nWeaponShootCount != 1)
						return;
					gPlayer[snum].nWeaponShootCount--;
					return;
				}
				if (currTime > 430) {
					gPlayer[snum].nWeaponSeq = 6;
					gPlayer[snum].nWeaponShootCount = 1;
					return;
				}
				if (currTime > 400) {
					gPlayer[snum].nWeaponSeq = 5;
					if (gPlayer[snum].nWeaponShootCount != 1)
						return;
					gPlayer[snum].nWeaponShootCount--;
					return;
				}
				if (currTime > 370) {
					gPlayer[snum].nWeaponSeq = 6;
					gPlayer[snum].nWeaponShootCount = 1;
					return;
				}
				if (currTime > 340) {
					gPlayer[snum].nWeaponSeq = 5;
					if (gPlayer[snum].nWeaponShootCount != 1)
						return;
					gPlayer[snum].nWeaponShootCount--;
					return;
				}
				if (currTime > 310) {
					gPlayer[snum].nWeaponSeq = 6;
					gPlayer[snum].nWeaponShootCount = 1;
					return;
				}
				if (currTime > 280) {
					gPlayer[snum].nWeaponSeq = 5;
					if (gPlayer[snum].nWeaponShootCount != 1)
						return;
					gPlayer[snum].nWeaponShootCount--;
					return;
				}
				if (currTime > 250) {
					gPlayer[snum].nWeaponSeq = 6;
					gPlayer[snum].nWeaponShootCount = 1;
					return;
				}
				if (currTime > 220) {
					gPlayer[snum].nWeaponSeq = 5;
					if (gPlayer[snum].nWeaponShootCount != 1)
						return;
					gPlayer[snum].nWeaponShootCount--;
					return;
				}
				if (currTime > 190) {
					gPlayer[snum].nWeaponSeq = 6;
					gPlayer[snum].nWeaponShootCount = 1;
					return;
				}
				if (currTime > 160) {
					gPlayer[snum].nWeaponSeq = 5;
					if (gPlayer[snum].nWeaponShootCount != 1)
						return;
					gPlayer[snum].nWeaponShootCount--;
					return;
				}
				if (currTime > 130) {
					gPlayer[snum].nWeaponSeq = 6;
					gPlayer[snum].nWeaponShootCount = 1;
					return;
				}
				if (currTime > 100) {
					gPlayer[snum].nWeaponSeq = 5;
					if (gPlayer[snum].nWeaponShootCount != 1)
						return;
					gPlayer[snum].nWeaponShootCount--;
					return;
				}
				if (currTime > 86) {
					gPlayer[snum].nWeaponSeq = 4;
					return;
				}
				if (currTime > 70) {
					gPlayer[snum].nWeaponSeq = 3;
					return;
				}
				if (currTime > 46) {
					gPlayer[snum].nWeaponSeq = 2;
					return;
				}
				if (currTime > 12) {
					gPlayer[snum].nWeaponSeq = 1;
					return;
				}
				gPlayer[snum].nWeaponSeq = 0;
				return;
			}
		}
		
		if (gPlayer[snum].nWeaponState == 0 && gPlayer[snum].nNewWeapon > 0 && gPlayer[snum].nNewWeapon != 999) {
			if (gPlayer[snum].isWeaponsSwitching == 0) {
				gPlayer[snum].isWeaponsSwitching = 1;
				gPlayer[snum].nSwitchingClock = lockclock;
			}
		}

		if (gPlayer[snum].isWeaponsSwitching != 0) {
			gPlayer[snum].nWeaponState = 0;
			if (gPlayer[snum].nNewWeapon >= 100) {
				weapy = 200 - 3 * (lockclock - gPlayer[snum].nSwitchingClock);
				if (weapy <= 0) {
					gPlayer[snum].isWeaponsSwitching = 0;
					weapy = 0;
				}
			} else {
				weapy = (lockclock - gPlayer[snum].nSwitchingClock) * 3;
				if (200 <= weapy) {
					gPlayer[snum].nLastWeapon = (short) (gPlayer[snum].nWeapon + 1);
					gPlayer[snum].nWeapon = (short) gPlayer[snum].nNewWeapon;
					gPlayer[snum].nNewWeapon = 999;
					gPlayer[snum].nSwitchingClock = lockclock;
					gPlayer[snum].nWeaponClock = lockclock;
				}
			}
		}
	}

	public static void weaponfire(int snum) {
		if (gPlayer[snum].nWeaponState == 0) {
			if (!isonwater(snum)) {
				gPlayer[snum].isWeaponFire = 0;
				gPlayer[snum].nWeaponShootCount = 0;
				gPlayer[snum].nWeaponState = 0;

				if (gPlayer[snum].nWeapon >= 13
						|| gPlayer[snum].nWeapon <= 6 && gPlayer[snum].nAmmo[gPlayer[snum].nWeapon] != 0
						|| gPlayer[snum].nMana > 0 && gPlayer[snum].nAmmo[gPlayer[snum].nWeapon] != 0) {
					gPlayer[snum].nWeaponClock = lockclock;
					gPlayer[snum].isWeaponFire = 1;
					gPlayer[snum].nWeaponShootCount = 1;
					gPlayer[snum].nWeaponState = 1;
					if (gPlayer[snum].nWeapon == 11)
						gPlayer[snum].nQigong5Phase = 0;
				}
			}
		}
	}
	
	public static void nextweapon(int snum)
	{
		int nCurrentWeapon = gPlayer[snum].nWeapon + 1;
		if(nCurrentWeapon > 13)
			nCurrentWeapon = 1;
		int i = nCurrentWeapon + 1;
		while(!switchweapon(snum, i)) {
			if(i++ >= 13)
				i = 1;
		}
	}
	
	public static void prevweapon(int snum)
	{
		int nCurrentWeapon = gPlayer[snum].nWeapon + 1;
		if(nCurrentWeapon > 13)
			nCurrentWeapon = 1;
		int i = nCurrentWeapon - 1;
		while(!switchweapon(snum, i)) {
			if(i-- <= 0)
				i = 13;
		}
	}

	public static boolean switchweapon(int snum, int nWeapon)
	{
		int nNewWeap = nWeapon - 1;
		if(nWeapon > 13)
			nWeapon = 1;
		
		switch(nWeapon)
		{
		case 1:
			if (gPlayer[snum].nWeapon != nPlayerFirstWeapon)
				gPlayer[snum].nNewWeapon = nPlayerFirstWeapon;
			return true;
		case 8: //red ball
		case 9: //orange ball
		case 10: //green balls
		case 11: //electric
		case 12: //fire
		case 13: //boomerang
			if (gPlayer[snum].nMana <= 0)
				break;
		case 2: //coin
		case 3: //ball
		case 4: //needle
		case 5: //shuriken
		case 6: //dagger
		case 7: //kunai
			if (gPlayer[snum].nWeapon != nNewWeap && gPlayer[snum].nAmmo[nNewWeap] != 0) {
				gPlayer[snum].nNewWeapon = nNewWeap;
				if(nWeapon >= 2 && nWeapon <= 7)
					gPlayer[snum].nLastChoosedWeapon = (short) nNewWeap;
				else if(nWeapon >= 8 && nWeapon <= 13) gPlayer[snum].nLastManaWeapon = (short) nNewWeap;
			}
			break;
		default:
			return false;
		}
		
		return gPlayer[snum].nNewWeapon != 999 || gPlayer[snum].nWeapon == nNewWeap;
	}

	public static boolean switchweapgroup(int snum, int nWeapon) {
		boolean changed = false;
		int nNewWeap, i;
		switch (nWeapon) {
		case 1:
			if (gPlayer[snum].nWeapon != nPlayerFirstWeapon)
				gPlayer[snum].nNewWeapon = nPlayerFirstWeapon;
			break;
		case 2:
			nNewWeap = gPlayer[snum].nLastChoosedWeapon + 1;
			if(gPlayer[snum].nWeapon > 6)
				nNewWeap = gPlayer[snum].nLastChoosedWeapon;
			if (nNewWeap > 6)
				nNewWeap = 1;

			for (i = 0; i < 6 && gPlayer[snum].nAmmo[nNewWeap] == 0; i++) {
				if (++nNewWeap > 6)
					nNewWeap = 1;
			}

			if (gPlayer[snum].nAmmo[nNewWeap] != 0) {
				if (i < 5 || nNewWeap != gPlayer[snum].nWeapon) {
					gPlayer[snum].nLastChoosedWeapon = (short) nNewWeap;
					gPlayer[snum].nNewWeapon = nNewWeap;
					changed = true;
				}
			}
			break;
		case 3:
			if (gPlayer[snum].nMana > 0) {
				nNewWeap = gPlayer[snum].nLastManaWeapon + 1;
				if(gPlayer[snum].nWeapon < 7 || gPlayer[snum].nWeapon > 12)
					nNewWeap = gPlayer[snum].nLastManaWeapon;
					
				if (nNewWeap > 12 || nNewWeap < 7)
					nNewWeap = 7;

				for (i = 0; i < 6 && gPlayer[snum].nAmmo[nNewWeap] == 0; i++) {
					if (++nNewWeap > 12)
						nNewWeap = 7;
				}

				if (i < 5 || nNewWeap != gPlayer[snum].nWeapon) {
					gPlayer[snum].nLastManaWeapon = (short) nNewWeap;
					gPlayer[snum].nNewWeapon = nNewWeap;
					playsound(55);
					changed = true;
				}
			}
			break;
		}

		return changed;
	}

	public static void weaponbobbing(int snum) {
		if (gPlayer[snum].isWeaponsSwitching == 0) {
			int displacement = 2 * (8 - Math.abs(8 - gPlayer[snum].nBobCount));
			weapx = displacement;
			weapy = displacement;
		}
	}

	record WeaponStateDraw(int x, int y, int offset) {}

	public static WeaponStateDraw[][][] WEAPON_DRAW_DATA = {
			{ // melee
					{ // MELEE1_HAND
							new WeaponStateDraw(134, 60, 0),
							new WeaponStateDraw(212, 28, 1),
							new WeaponStateDraw(155, 80, 2),
							new WeaponStateDraw(49, 77, 3),
							new WeaponStateDraw(0, 95, 4),
							new WeaponStateDraw(0, 129, 5),
							new WeaponStateDraw(0, 158, 6),
					},
					{ // MELEE2_HAND
							new WeaponStateDraw(41, 47, 0),
							new WeaponStateDraw(0, 124, 1),
							new WeaponStateDraw(83, 129, 2),
							new WeaponStateDraw(123, 136, 3),
							new WeaponStateDraw(247, 165, 4),
							new WeaponStateDraw(247, 165, 4),
					},
					{ // MELEE3_HAND
							new WeaponStateDraw(46, 65, 0),
							new WeaponStateDraw(0, 49, 1),
							new WeaponStateDraw(23, 54, 2),
							new WeaponStateDraw(46, 65, 3),
							new WeaponStateDraw(46, 65, 3),
							new WeaponStateDraw(46, 65, 3),
					},
					{ // MELEE4_HAND
							new WeaponStateDraw(0, 133, 0),
							new WeaponStateDraw(0, 108, 1),
							new WeaponStateDraw(103, 102, 2),
							new WeaponStateDraw(168, 111, 3),
							new WeaponStateDraw(240, 132, 4),
					},
					{ // MELEE5_HAND
							new WeaponStateDraw(190, 107, 0),
							new WeaponStateDraw(252, 37, 1),
							new WeaponStateDraw(152, 65, 2),
							new WeaponStateDraw(49, 60, 3),
							new WeaponStateDraw(0, 85, 4),
							new WeaponStateDraw(0, 128, 5),
					},
					{ // MELEE6_HAND
							new WeaponStateDraw(202, 132, 0),
							new WeaponStateDraw(195, 135, 1),
							new WeaponStateDraw(149, 118, 2),
							new WeaponStateDraw(143, 112, 3),
					},
					{ // MELEE7_HAND
							new WeaponStateDraw(173, 48, 0),
							new WeaponStateDraw(78, 105, 1),
							new WeaponStateDraw(34, 108, 2),
							new WeaponStateDraw(0, 109, 3),
							new WeaponStateDraw(0, 133, 4),
					},
					{ // MELEE8_HAND
							new WeaponStateDraw(167, 108, 0),
							new WeaponStateDraw(252, 37, 1),
							new WeaponStateDraw(152, 75, 2),
							new WeaponStateDraw(60, 72, 3),
							new WeaponStateDraw(0, 96, 4),
							new WeaponStateDraw(0, 140, 5),
					},
			},
			{ // anqi
					{ // ANQI1_HAND
							new WeaponStateDraw(146, 99, 0),
							new WeaponStateDraw(109, 101, 1),
							new WeaponStateDraw(46, 125, 2),
							new WeaponStateDraw(28, 150, 3),
							new WeaponStateDraw(0, 180, 4),
							new WeaponStateDraw(243, 155, 5),
					},
					{ // ANQI2_HAND
							new WeaponStateDraw(6, 83, 0),
							new WeaponStateDraw(78, 106, 1),
							new WeaponStateDraw(157, 125, 2),
							new WeaponStateDraw(215, 150, 3),
							new WeaponStateDraw(271, 177, 4),
							new WeaponStateDraw(0, 151, 5),
					},
					{ // ANQI3_HAND
							new WeaponStateDraw(147, 57, 0),
							new WeaponStateDraw(78, 96, 1),
							new WeaponStateDraw(50, 139, 2),
							new WeaponStateDraw(4, 161, 3),
							new WeaponStateDraw(0, 179, 4),
							new WeaponStateDraw(263, 122, 5),
					},
					{ // ANQI4_HAND
							new WeaponStateDraw(134, 68, 0),
							new WeaponStateDraw(78, 104, 1),
							new WeaponStateDraw(50, 139, 2),
							new WeaponStateDraw(4, 161, 3),
							new WeaponStateDraw(0, 179, 4),
							new WeaponStateDraw(194, 115, 5),
					},
					{ // ANQI5_HAND
							new WeaponStateDraw(12, 34, 0),
							new WeaponStateDraw(24, 105, 1),
							new WeaponStateDraw(133, 139, 2),
							new WeaponStateDraw(205, 161, 3),
							new WeaponStateDraw(273, 179, 4),
							new WeaponStateDraw(0, 104, 5),
					},
					{ // ANQI6_HAND
							new WeaponStateDraw(88, 64, 0),
							new WeaponStateDraw(78, 89, 1),
							new WeaponStateDraw(56, 120, 2),
							new WeaponStateDraw(35, 145, 3),
							new WeaponStateDraw(0, 176, 4),
							new WeaponStateDraw(182, 119, 5),
					},
			},
			{ // qigong
					{ // QIGONG1_HAND
							new WeaponStateDraw(116, 123, 0),
							new WeaponStateDraw(78, 104, 3),
							new WeaponStateDraw(241, 132, 4),
							new WeaponStateDraw(177, 132, 5),
							new WeaponStateDraw(138, 123, 6),
							new WeaponStateDraw(104, 98, 7),
							new WeaponStateDraw(101, 94, 8),
					},
					{ // QIGONG2_HAND
							new WeaponStateDraw(25, 116, 0),
							new WeaponStateDraw(29, 135, 3),
							new WeaponStateDraw(0, 145, 4),
							new WeaponStateDraw(0, 127, 5),
							new WeaponStateDraw(7, 111, 6),
							new WeaponStateDraw(45, 96, 7),
							new WeaponStateDraw(44, 93, 8),
					},
					{ // QIGONG3_HAND
							new WeaponStateDraw(111, 118, 0),
							new WeaponStateDraw(139, 135, 3),
							new WeaponStateDraw(241, 145, 4),
							new WeaponStateDraw(179, 127, 5),
							new WeaponStateDraw(134, 116, 6),
							new WeaponStateDraw(104, 98, 7),
							new WeaponStateDraw(101, 94, 8),
					},
					{ // QIGONG4_HAND
							new WeaponStateDraw(101, 117, 0),
							new WeaponStateDraw(160, 135, 3),
							new WeaponStateDraw(241, 145, 4),
							new WeaponStateDraw(154, 125, 5),
							new WeaponStateDraw(128, 114, 6),
							new WeaponStateDraw(101, 100, 7),
							new WeaponStateDraw(101, 94, 8),
					},
					{ // QIGONG5_HAND
							new WeaponStateDraw(0, 132, 0),
							new WeaponStateDraw(0, 132, 1),
							new WeaponStateDraw(0, 129, 2),
							new WeaponStateDraw(0, 146, 3),
							new WeaponStateDraw(0, 130, 4),
							new WeaponStateDraw(0, 128, 5),
							new WeaponStateDraw(0, 108, 6),
							new WeaponStateDraw(0, 97, 7),
					},
					{ // QIGONG6_HAND
							new WeaponStateDraw(0, 128, 0),
							new WeaponStateDraw(0, 128, 1),
							new WeaponStateDraw(0, 124, 2),
							new WeaponStateDraw(82, 135, 3),
							new WeaponStateDraw(44, 142, 4),
							new WeaponStateDraw(0, 142, 5),
							new WeaponStateDraw(0, 139, 6),
							new WeaponStateDraw(0, 135, 7),
							new WeaponStateDraw(0, 139, 8),
							new WeaponStateDraw(0, 142, 9), // y was 1442, but I believe it is 142.
							new WeaponStateDraw(0, 134, 10),
							new WeaponStateDraw(0, 122, 11),
							new WeaponStateDraw(0, 111, 12),
							new WeaponStateDraw(0, 96, 13),
							new WeaponStateDraw(0, 89, 14),
							new WeaponStateDraw(0, 89, 15), // Qigong #6 actually has a pic at offset=15
					},
			},
	};

	public static void drawweapons(int snum) {
		if (cfg.gCrosshair) {
//			overwritesprite(160, 100, 350, 0, 0, 2 | 8);
			int col = 4;
			engine.getrender().drawline256((xdim - mulscale(cfg.gCrossSize, 16, 16)) << 11, ydim << 11,
					(xdim - mulscale(cfg.gCrossSize, 4, 16)) << 11, ydim << 11, col);
			engine.getrender().drawline256((xdim + mulscale(cfg.gCrossSize, 4, 16)) << 11, ydim << 11,
					(xdim + mulscale(cfg.gCrossSize, 16, 16)) << 11, ydim << 11, col);
			engine.getrender().drawline256(xdim << 11, (ydim - mulscale(cfg.gCrossSize, 16, 16)) << 11, xdim << 11,
					(ydim - mulscale(cfg.gCrossSize, 4, 16)) << 11, col);
			engine.getrender().drawline256(xdim << 11, (ydim + mulscale(cfg.gCrossSize, 4, 16)) << 11, xdim << 11,
					(ydim + mulscale(cfg.gCrossSize, 16, 16)) << 11, col);
		}

		int curr = gPlayer[snum].nWeapon;
		int weaponTile = nWeaponTile[curr];
		WeaponStateDraw[] drawDataForSeq = switch(weaponTile) {
			case MELEE1_HAND -> WEAPON_DRAW_DATA[0][0];
			case MELEE2_HAND -> WEAPON_DRAW_DATA[0][1];
			case MELEE3_HAND -> WEAPON_DRAW_DATA[0][2];
			case MELEE4_HAND -> WEAPON_DRAW_DATA[0][3];
			case MELEE5_HAND -> WEAPON_DRAW_DATA[0][4];
			case MELEE6_HAND -> WEAPON_DRAW_DATA[0][5];
			case MELEE7_HAND -> WEAPON_DRAW_DATA[0][6];
			case MELEE8_HAND -> WEAPON_DRAW_DATA[0][7];
			case ANQI1_HAND -> WEAPON_DRAW_DATA[1][0];
			case ANQI2_HAND -> WEAPON_DRAW_DATA[1][1];
			case ANQI3_HAND -> WEAPON_DRAW_DATA[1][2];
			case ANQI4_HAND -> WEAPON_DRAW_DATA[1][3];
			case ANQI5_HAND -> WEAPON_DRAW_DATA[1][4];
			case ANQI6_HAND -> WEAPON_DRAW_DATA[1][5];
			case QIGONG1_HAND -> WEAPON_DRAW_DATA[2][0];
			case QIGONG2_HAND -> WEAPON_DRAW_DATA[2][1];
			case QIGONG3_HAND -> WEAPON_DRAW_DATA[2][2];
			case QIGONG4_HAND -> WEAPON_DRAW_DATA[2][3];
			case QIGONG5_HAND -> WEAPON_DRAW_DATA[2][4];
			case QIGONG6_HAND -> WEAPON_DRAW_DATA[2][5];
			default ->
				throw new RuntimeException("Unrecognized weapon: " + weaponTile);
		};

		int seq = gPlayer[snum].nWeaponState == 0 ? 0 : gPlayer[snum].nWeaponSeq;
		WeaponStateDraw data = drawDataForSeq[seq];

		if (gPlayer[snum].nWeaponState == 0) {
			final boolean handGlow;
			final int glowInterval;
			final int glowFrames;

			switch (weaponTile) {
				case QIGONG2_HAND:
				case QIGONG3_HAND:
				case QIGONG4_HAND:
					handGlow = true;
					glowInterval = 2;
					glowFrames = 4;
					break;
				case QIGONG5_HAND:
				case QIGONG6_HAND:
					handGlow = true;
					glowInterval = 4;
					glowFrames = 4;
					break;
				default:
					handGlow = false;
					glowInterval = 0;
					glowFrames = 0;
			}

			final int glowOffset;
			if (handGlow) {
				int frame = totalmoves / glowInterval % glowFrames;
				glowOffset = Math.abs(frame - glowFrames / 2);
			} else {
				glowOffset = 0;
			}

			overwritesprite(weapx + data.x, weapy + data.y, weaponTile + glowOffset, 0, 0, 2 | 8 | 16);
		} else {
			overwritesprite(data.x, data.y, weaponTile + data.offset, 0, 0, 2 | 8 | 16);
		}
	}

	public static void shoot(int plr, int x, int y, int z, int ang, int horiz, short sectnum, int opt) {
		final boolean CLASSIC_PROJECTILE = cfg.projectileGravity == Config.ProjectileGravity.CLASSIC;
		final int ALTERNATIVE_PROJECTILE_ZOFFSET = 0xC00;

		short spr;
		switch (opt) {
		case 0:
			engine.hitscan(x, y, z, sectnum, sintable[(ang + 512) & 0x7FF], sintable[ang & 0x7FF], 2000 * (100 - horiz),
					pHitInfo, CLIPMASK0);

			if (pHitInfo.hitsect >= 0 && pHitInfo.hitsprite < 0) {
				if (Math.abs(pHitInfo.hitx - x) + Math.abs(pHitInfo.hity - y) < 600) {
					spr = engine.insertsprite(pHitInfo.hitsect, SHOTSPARK);

					sprite[spr].x = pHitInfo.hitx;
					sprite[spr].y = pHitInfo.hity;
					sprite[spr].z = pHitInfo.hitz + 2560;
					sprite[spr].cstat = 2 | 128;
					sprite[spr].picnum = 1341;
					sprite[spr].shade = -4;
					sprite[spr].xrepeat = 40;
					sprite[spr].yrepeat = 40;
					sprite[spr].ang = (short) ang;
					sprite[spr].xvel = 0;
					sprite[spr].yvel = 0;
					sprite[spr].zvel = 0;
					sprite[spr].owner = (short) (plr + 4096);
					sprite[spr].lotag = 63;

					// engine.movesprite(spr, -(sintable[(ang + 512) & 0x7FF] >> 7), -(sintable[ang]
					// >> 7), 0, 128, 1024, 1024, CLIPMASK1, 4);
				}
			} else if (pHitInfo.hitsprite >= 0) {
				if (Math.abs(pHitInfo.hitx - x) + Math.abs(pHitInfo.hity - y) < 600) {
					SPRITE en = sprite[pHitInfo.hitsprite];

					if (isAnyDudeSprite(en.picnum)) {
						en.lotag -= gPlayer[plr].nFirstWeaponDamage;
						if (en.lotag <= 0) {
							if (isDudeSprite(en.picnum, PURPLEDUDE) || isDudeSprite(en.picnum, BLUEDUDE))
								playsound(14, pHitInfo.hitsprite);
							else if (isDudeSprite(en.picnum, YELLOWDUDE))
								playsound(18, pHitInfo.hitsprite);
							else
								playsound(17, pHitInfo.hitsprite);
							enemydie(pHitInfo.hitsprite);
						} else {
							Enemies.enemyHit(pHitInfo.hitsprite);
						}
					}
				}
			}

			gPlayer[plr].nMeleePhase++;
			gPlayer[plr].nMeleePhase &= 3;
			playsound(45 + gPlayer[plr].nMeleePhase);
			return;
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
			spr = engine.insertsprite(sectnum, PROJECTILE);
			sprite[spr].x = x;
			sprite[spr].y = y;
			sprite[spr].z = z + (CLASSIC_PROJECTILE ? 3328 : ALTERNATIVE_PROJECTILE_ZOFFSET);
			sprite[spr].clipdist = 16;
			sprite[spr].cstat = 128;
			sprite[spr].picnum = (short) (nWeaponTile[gPlayer[plr].nWeapon] + 6);
			sprite[spr].shade = -24;
			sprite[spr].xrepeat = 64;
			sprite[spr].yrepeat = 64;
			sprite[spr].ang = (short) ang;
			sprite[spr].xvel = (short) (sintable[(ang + 2560) & 0x7FF] >> 5);
			sprite[spr].yvel = (short) (sintable[(ang + 2048) & 0x7FF] >> 5);
			sprite[spr].zvel = (short) ((100 - horiz) << 6);
			sprite[spr].owner = (short) (plr + 4096);

			if (opt >= 4)
				sprite[spr].lotag = (short) (8 * opt - 24);
			else
				sprite[spr].lotag = (short) (opt + 4);

			if (opt == 2) {
				sprite[spr].hitag = ANQI2_MAX_BOUNCE;
			}

			if (cfg.bRevisedAnqi4 && opt == 4) {
				double SIZE_MULTIPLER = 1.5;
				double VELOCITY_MULTIPLER = 0.75;
				sprite[spr].xrepeat *= SIZE_MULTIPLER;
				sprite[spr].yrepeat *= SIZE_MULTIPLER;
				sprite[spr].xvel *= VELOCITY_MULTIPLER;
				sprite[spr].yvel *= VELOCITY_MULTIPLER;
				sprite[spr].zvel *= VELOCITY_MULTIPLER;
			}

			changeammo(plr, opt, -1);
			if (gPlayer[plr].nAmmo[opt] == 0) {
				if(!switchweapgroup(plr, 2))
					switchweapgroup(plr, 1);
			}
			playsound(80 + (opt - 1));
			return;
		case 7:
		case 8:
			playsound(36);
			spr = engine.insertsprite(sectnum, PROJECTILE);
			sprite[spr].x = x;
			sprite[spr].y = y;
			sprite[spr].z = z + (CLASSIC_PROJECTILE ? 3584 : ALTERNATIVE_PROJECTILE_ZOFFSET);
			sprite[spr].cstat = 128;
			sprite[spr].picnum = (short) (nWeaponTile[gPlayer[plr].nWeapon] + 9);
			sprite[spr].shade = -23;
			sprite[spr].xrepeat = 48;
			sprite[spr].yrepeat = 48;
			sprite[spr].ang = (short) ang;
			sprite[spr].xvel = (short) (sintable[(ang + 2560) & 0x7FF] >> 5);
			sprite[spr].yvel = (short) (sintable[(ang + 2048) & 0x7FF] >> 5);
			sprite[spr].zvel = (short) (50 * (100 - horiz));
			sprite[spr].owner = (short) (plr + 4096);
			sprite[spr].lotag = (short) (8 * opt - 48);
			changemanna(plr, -2);
			if (opt == 7)
				playsound(57);
			else
				playsound(58);
			if (gPlayer[plr].nMana == 0) {
				if(!switchweapgroup(plr, 3) && !switchweapgroup(plr, 2))
					switchweapgroup(plr, 1);
			}
			return;
		case 9:
			playsound(36);
			short anga = (short) (ang + 1856);
			for (int i = 0; i < 7; i++) {
				spr = engine.insertsprite(sectnum, PROJECTILE);
				sprite[spr].x = x;
				sprite[spr].y = y;
				sprite[spr].z = z + (CLASSIC_PROJECTILE ? 5120 : ALTERNATIVE_PROJECTILE_ZOFFSET);
				sprite[spr].cstat = 128;
				sprite[spr].picnum = QIGONG3_PROJ;
				sprite[spr].shade = -23;
				sprite[spr].xrepeat = 24;
				sprite[spr].yrepeat = 24;
				sprite[spr].ang = anga;
				sprite[spr].xvel = (short) (sintable[(anga + 512) & 0x7FF] >> 5);
				sprite[spr].yvel = (short) (sintable[anga & 0x7FF] >> 5);
				sprite[spr].zvel = (short) (50 * (100 - horiz));
				sprite[spr].owner = (short) (plr + 4096);
				sprite[spr].lotag = 5;
				anga += 64;
			}
			changemanna(plr, -7);
			playsound(59);
			if (gPlayer[plr].nMana == 0) {
				if(!switchweapgroup(plr, 3) && !switchweapgroup(plr, 2))
					switchweapgroup(plr, 1);
			}
			return;
		case 10:
			playsound(36);
			if (cfg.bRevisedQigong4) {
				// Create a bigger lightning ball that will explode into small ones.
				spr = engine.insertsprite(sectnum, PROJECTILE);
				sprite[spr].x = x;
				sprite[spr].y = y;
				sprite[spr].z = z + (CLASSIC_PROJECTILE ? 3584 : ALTERNATIVE_PROJECTILE_ZOFFSET);
				sprite[spr].cstat = 128;
				sprite[spr].picnum = QIGONG4_PROJ;
				sprite[spr].shade = -23;
				sprite[spr].xrepeat = 72;
				sprite[spr].yrepeat = 72;
				sprite[spr].ang = (short) ang;
				sprite[spr].xvel = (short) (sintable[(ang + 2560) & 0x7FF] >> 5);
				sprite[spr].yvel = (short) (sintable[(ang + 2048) & 0x7FF] >> 5);
				sprite[spr].zvel = (short) (50 * (100 - horiz));
				sprite[spr].owner = (short) (plr + 4096);
				sprite[spr].lotag = 5;
				sprite[spr].hitag = QIGONG4_HITAG_BIG;
			} else {
				// The coordinate where the lightning balls scatter from.
				// In the classic mode, the lightning balls are created 1024 units
				// in front of the player.
				int centerX = (sintable[(ang + 512) & 0x7FF] >> 4) + x;
				int centerY = (sintable[ang & 0x7FF] >> 4) + y;
				int centerZ = z + (CLASSIC_PROJECTILE ? 5120 : ALTERNATIVE_PROJECTILE_ZOFFSET);
				int owner = plr + 4096;
				createQigong4Ring(false, centerX, centerY, centerZ, ang, horiz, sectnum, (short)owner);
			}
			changemanna(plr, -10);
			playsound(60);
			if (gPlayer[plr].nMana == 0) {
				if(!switchweapgroup(plr, 3) && !switchweapgroup(plr, 2))
					switchweapgroup(plr, 1);
			}
			return;
		case 11:
			playsound(36);
			if (gPlayer[plr].nQigong5Phase == 5 || gPlayer[plr].nQigong5Phase == 6)
				playsound(61);

			final int HORIZONTAL_OFFSET_SHIFT;
			final int VERTICAL_OFFSET;
			final int HORIZONTAL_VELOCITY_SHIFT;
			final short zVelocity;
			
			if (cfg.bRevisedQigong5) {
				HORIZONTAL_OFFSET_SHIFT = 7;
				VERTICAL_OFFSET = ALTERNATIVE_PROJECTILE_ZOFFSET;
				HORIZONTAL_VELOCITY_SHIFT = 5;
				zVelocity = (short) ((100 - horiz) << 6);
			} else {
				HORIZONTAL_OFFSET_SHIFT = 5;
				VERTICAL_OFFSET = 0x2000;
				HORIZONTAL_VELOCITY_SHIFT = 8;
				zVelocity = (short) (50 * (100 - horiz));
			}

			spr = engine.insertsprite(sectnum, PROJECTILE);
			sprite[spr].x = x + (sintable[(ang + 2816) & 0x7FF] >> HORIZONTAL_OFFSET_SHIFT);
			sprite[spr].y = y + (sintable[(ang + 2304) & 0x7FF] >> HORIZONTAL_OFFSET_SHIFT);
			sprite[spr].z = z + VERTICAL_OFFSET;
			sprite[spr].cstat = 128;
			sprite[spr].picnum = (short) (QIGONG5_PROJ + gPlayer[plr].nQigong5Phase);
			sprite[spr].shade = -23;
			sprite[spr].xrepeat = 32;
			sprite[spr].yrepeat = 32;
			sprite[spr].ang = (short) ang;
			sprite[spr].xvel = (short) (sintable[(ang + 2560) & 0x7FF] >> HORIZONTAL_VELOCITY_SHIFT);
			sprite[spr].yvel = (short) (sintable[(ang + 2048) & 0x7FF] >> HORIZONTAL_VELOCITY_SHIFT);
			sprite[spr].zvel = zVelocity;
			sprite[spr].owner = (short) (plr + 4096);
			sprite[spr].lotag = 48;
			if (++gPlayer[plr].nQigong5Phase > 8)
				gPlayer[plr].nQigong5Phase = 0;

			spr = engine.insertsprite(sectnum, PROJECTILE);
			sprite[spr].x = (sintable[(ang + 2304) & 0x7FF] >> HORIZONTAL_OFFSET_SHIFT) + x;
			sprite[spr].y = (sintable[(ang + 1792) & 0x7FF] >> HORIZONTAL_OFFSET_SHIFT) + y;
			sprite[spr].z = z + VERTICAL_OFFSET;
			sprite[spr].cstat = 128;
			sprite[spr].picnum = (short) (QIGONG5_PROJ + gPlayer[plr].nQigong5Phase);
			sprite[spr].shade = -23;
			sprite[spr].xrepeat = 32;
			sprite[spr].yrepeat = 32;
			sprite[spr].ang = (short) ang;
			sprite[spr].xvel = (short) (sintable[(ang + 2560) & 0x7FF] >> HORIZONTAL_VELOCITY_SHIFT);
			sprite[spr].yvel = (short) (sintable[(ang + 2048) & 0x7FF] >> HORIZONTAL_VELOCITY_SHIFT);
			sprite[spr].zvel = zVelocity;
			sprite[spr].owner = (short) (plr + 4096);
			sprite[spr].lotag = 48;
			if (++gPlayer[plr].nQigong5Phase > 8)
				gPlayer[plr].nQigong5Phase = 0;
			changemanna(plr, -2);
			if (gPlayer[plr].nMana == 0) {
				if(!switchweapgroup(plr, 3) && !switchweapgroup(plr, 2))
					switchweapgroup(plr, 1);
			}
			return;
		case 12:
			int projectileZ = z + (CLASSIC_PROJECTILE ? 3584 : ALTERNATIVE_PROJECTILE_ZOFFSET);

			playsound(36);
			spr = engine.insertsprite(sectnum, PROJECTILE);
			sprite[spr].x = x;
			sprite[spr].y = y;
			sprite[spr].z = projectileZ;
			sprite[spr].cstat = 128;
			sprite[spr].picnum = QIGONG6_PROJ;
			sprite[spr].shade = -23;
			sprite[spr].xrepeat = 64;
			sprite[spr].yrepeat = 64;
			if (cfg.bRevisedQigong6) {
				double vx = sintable[(ang + 2560) & 0x7FF];
				double vy = sintable[(ang + 2048) & 0x7FF];
				Optional<AutoTargetInfo> maybeTargetInfo = Sprites.qigong6AutoTarget(
						x, y, projectileZ, sectnum, TargetingMode.ANGLE, vx, vy, QIGONG6_AIM_COS, -1);
				if (maybeTargetInfo.isEmpty()) {
					sprite[spr].ang = (short) ang;
					sprite[spr].xvel = (short) (sintable[(ang + 2560) & 0x7FF] >> 5);
					sprite[spr].yvel = (short) (sintable[(ang + 2048) & 0x7FF] >> 5);
					sprite[spr].zvel = (short)((100 - horiz) << 6);
				} else {
					AutoTargetInfo ti = maybeTargetInfo.get();
					setQigong6Velocity(sprite[spr], ti.dx(), ti.dy(), ti.dz());
				}
				sprite[spr].hitag = QIGONG6_BOUNCING_POWER;
			} else {
				if (headspritestat[CHASE] == -1) {
					sprite[spr].ang = (short) ang;
					short v49 = sprite[spr].ang;
					sprite[spr].xvel = (short) (sintable[(v49 + 2560) & 0x7FF] >> 5);
					sprite[spr].yvel = (short) (sintable[(v49 + 2048) & 0x7FF] >> 5);
				} else {
					short v44 = headspritestat[CHASE];
					if (sprite[v44].picnum == BLUEDUDE || sprite[v44].picnum == GREENDUDE || sprite[v44].picnum == REDDUDE
							|| sprite[v44].picnum == PURPLEDUDE || sprite[v44].picnum == YELLOWDUDE) {
						sprite[spr].ang = engine.getangle(sprite[v44].x - gPlayer[plr].x, sprite[v44].y - gPlayer[plr].y);
						sprite[spr].xvel = (short) (sintable[(sprite[spr].ang + 2560) & 0x7FF] >> 5);
						sprite[spr].yvel = (short) (sintable[(sprite[spr].ang + 2048) & 0x7FF] >> 5);
					} else {
						sprite[spr].ang = (short) ((ang - 128) & 0x7FF);
						sprite[spr].xvel = (short) (sintable[(sprite[spr].ang + 2560) & 0x7FF] >> 5);
						sprite[spr].yvel = (short) (sintable[(sprite[spr].ang + 2048) & 0x7FF] >> 5);
					}
				}
				sprite[spr].zvel = (short) (50 * (100 - horiz));
				sprite[spr].hitag = 0;
			}
			sprite[spr].owner = (short) (plr + 4096);
			sprite[spr].lotag = 54;
			changemanna(plr, -20);
			playsound(62);
			if (gPlayer[plr].nMana == 0) {
				if(!switchweapgroup(plr, 3) && !switchweapgroup(plr, 2))
					switchweapgroup(plr, 1);
			}
			return;
		}
	}

	public static void createQigong4Ring(boolean explodedFromBig, int x, int y, int z, int ang, int horiz, short sectnum, short owner) {
		final short damage;
		final int speedShift;
		final int nBallsPerSide;
		final int angBetweenBalls;
		final short hitag;

		if (explodedFromBig) {
			// In the revised game, it explodes into more balls, but each ball deals less damage.
			// The main damage source is now the explosion damage.
			damage = 4;
			speedShift = 4;
			nBallsPerSide = 15;
			angBetweenBalls = 64;
			hitag = QIGONG4_HITAG_SMALL;
		} else {
			// The classic game generates 9 balls in the range of -135 degrees to 135 degrees.
			damage = 10;
			speedShift = 6;
			nBallsPerSide = 4;
			angBetweenBalls = 192;
			hitag = QIGONG4_HITAG_CLASSIC;
		}

		final int maxAbsAng = nBallsPerSide * angBetweenBalls;
		final int nBalls = nBallsPerSide * 2 + 1;

		// Assert the classic game has 9 balls.
		assert explodedFromBig || nBalls == 9;

		ang = (ang + 2048 - maxAbsAng) % 2048;

		for (int i = 0; i < nBalls; i++) {
			int spr = engine.insertsprite(sectnum, PROJECTILE);
			sprite[spr].x = x;
			sprite[spr].y = y;
			sprite[spr].z = z;
			sprite[spr].cstat = 128;
			sprite[spr].picnum = QIGONG4_PROJ;
			sprite[spr].shade = -24;
			sprite[spr].xrepeat = 24;
			sprite[spr].yrepeat = 24;
			sprite[spr].ang = (short) ang;
			sprite[spr].xvel = (short) (sintable[(ang + 512) & 0x7FF] >> speedShift);
			sprite[spr].yvel = (short) (sintable[ang & 0x7FF] >> speedShift);
			sprite[spr].zvel = (short) (50 * (100 - horiz));
			sprite[spr].owner = owner;
			sprite[spr].lotag = damage;
			sprite[spr].hitag = hitag;
			ang += angBetweenBalls;
		}
	}
}
