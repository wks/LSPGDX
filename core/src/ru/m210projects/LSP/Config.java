// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP;

import static ru.m210projects.Build.OnSceenDisplay.Console.OSDTEXT_YELLOW;
import static ru.m210projects.Build.Input.Keymap.KEY_CAPSLOCK;
import static ru.m210projects.Build.Input.Keymap.MOUSE_LBUTTON;
import static ru.m210projects.Build.Input.Keymap.MOUSE_RBUTTON;
import static ru.m210projects.Build.Input.Keymap.MOUSE_WHELLDN;
import static ru.m210projects.Build.Input.Keymap.MOUSE_WHELLUP;
import static ru.m210projects.Build.Gameutils.*;


import com.badlogic.gdx.Input.Keys;

import ru.m210projects.Build.FileHandle.FileResource;
import ru.m210projects.Build.OnSceenDisplay.Console;
import ru.m210projects.Build.Settings.BuildConfig;
import ru.m210projects.LSP.Menus.SimpleMenuBuilder;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;

public class Config extends BuildConfig {

	@Target(ElementType.FIELD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface InConfigFile {
		String key();
	}

	public boolean gAutoRun;
	public int gOverlayMap;
	public boolean gShowMessages;
	public boolean gCrosshair;
	public int gCrossSize;
	public int gShowStat;
	public int gHUDSize;
	public int showMapInfo;
	public boolean bHeadBob;
	public boolean bOriginal;
	public boolean bShowExit;

	/**
	 * Modify the behaviour of Anqi #2, i.e. Iron Balls, so that the balls will ricochet from walls.
	 * <p>
	 * This makes the Iron Balls a very good weapon to use in narrow corridors.
	 */
	@SimpleMenuBuilder.InMenu(scope = "gamemod", title = "Revise Anqi #2")
	@InConfigFile(key = "RevisedAnqi2")
	public boolean bRevisedAnqi2;

	/**
	 * Modify the behaviour of Anqi #3, i.e. Sleeve Needles, so that it shoots in three-shot volleys.
	 * <p>
	 * This gives the Sleeve Needles 3x the DPS compared with its original version,
	 * making it a more deadly but safer alternative to the powerful Qigong moves in narrow corridors.
	 * Beware, however, for this weapon now drains ammo way faster than before.
	 * Save it for the more powerful foes.
	 */
	@SimpleMenuBuilder.InMenu(scope = "gamemod", title = "Revise Anqi #3")
	@InConfigFile(key = "RevisedAnqi3")
	public boolean bRevisedAnqi3;

	/**
	 * Modify the behaviour of Anqi #4, i.e. Cross Darts, so that it pierces through enemies.
	 * <p>
	 * Cross Darts now pierce through all sprites until hitting a wall.
	 * This makes it well-suited against tightly-packed enemies,
	 * and it is way safer than any qigong in narrow corridors.
	 * The drawback, however, is that it travels slower,
	 * and its damage is relatively low against stronger foes.
	 */
	@SimpleMenuBuilder.InMenu(scope = "gamemod", title = "Revise Anqi #4")
	@InConfigFile(key = "RevisedAnqi4")
	public boolean bRevisedAnqi4;

	/**
	 * Modify the behaviour of Qigong #4 to a big lightning ball exploding into smaller balls.
	 * <p>
	 * The 4th Qigong now shoots a single bigger lightning ball forward.
	 * On impact, it will explode into a ring of small lightning balls, denser than the original game.
	 * This design makes it suitable for clearing enemies hiding in a cave.
	 * <p>
	 * The small lightning balls move very fast, forming a ring of shock wave.
	 * This makes the Qigong live up to its name "Thunder Weighing a Thousand Jun" (雷霆千鈞 in Chinese).
	 * <p>
	 * FYI, a "jun" is a unit of weight in ancient China, roughly equivalent to 30 pounds.
	 * A thousand jun will be about 15 tons.
	 * But a more common saying in Chinese is "... like a thunder weighing <b>ten</b> thousand jun" (雷霆萬鈞)
	 * which is a metaphor for an unstoppable force.
	 */
	@SimpleMenuBuilder.InMenu(scope = "gamemod", title = "Revise Qigong #4")
	@InConfigFile(key = "RevisedQigong4")
	public boolean bRevisedQigong4;

	/**
	 * Modify the behaviour of Qigong #5 to make it faster and more accurate.
	 * <p>
	 * The projectiles now fly faster.  The z-velocity is adjusted so that it hits where the cursor points at.
	 * Each pair of projectiles are brought closer together so that they won't miss the enemy right at the cursor.
	 * This qigong now behaves more or less like the Devastator Weapon in Duke3D.
	 * The projectile damage remains as high as before.  One projectile is enough to kill a Red Dude, and a volley of
	 * projectiles can kill even the toughest Yellow Dude.  It is the second most powerful Qigong anyway.
	 */
	@SimpleMenuBuilder.InMenu(scope = "gamemod", title = "Revise Qigong #5")
	@InConfigFile(key = "RevisedQigong5")
	public boolean bRevisedQigong5;

	/**
	 * Modify the behaviour of Qigong #6 to make it bounce from one enemy to another.
	 * <p>
	 * The original game manual says this Qigong is "碰撞彈射跟蹤單發", i.e. it shoots a single projectile that collides,
	 * ricochets, and is guided.  But in the actual game, it is not guided, but travels in a circle, which makes it
	 * almost impossible to hit any enemy.  It only bounces from the wall, but disappears after hitting a single
	 * enemy, and it costs 20 mana to cast.  Such a skill is almost useless.
	 * <p>
	 * With this modification, it automatically searches for another enemy when hitting an enemy, a wall or any sprite.
	 * If it can't find another target, it will bounce off walls like the metal balls (Anqi #2).
	 * It is supposed to be the player's ultimate Qigong.
	 * When cast appropriately, it can hit and kill up to ten Red Dudes in one shot without having to aim.
	 * However, due to its enormous mana cost, it should only be used sparingly.
	 * <p>
	 * The name of this Qigong is "怒月劈星", literally "furious moon slashing stars".  From my understanding, the
	 * "furious moon" refers to the powerful boomerang-shaped projectile, and the "stars" refer to the enemies it
	 * travels through.
	 */
	@SimpleMenuBuilder.InMenu(scope = "gamemod", title = "Revise Qigong #6")
	@InConfigFile(key = "RevisedQigong6")
	public boolean bRevisedQigong6;

	/**
	 * Enable the armor system.
	 * <p>
	 * The original L7P game doesn't have a working armor system, but there are armor items, including Parasol, Shield
	 * and Gold-thread Armor.  In the original game, they simply increase the player's health.  We modify the design so
	 * that they increase a new attribute, "armor", instead.  Every point of armor can absorb 1 point of damage just
	 * like health.  The maximum armor a player can have is also 160, but a player can acquire health and armor
	 * independently.  With full health and armor, the player can take 320 damage before dying.
	 * <p>
	 * Armor will absorb 50% of the damage for the player, but cannot prevent the player from death when the health
	 * reaches zero.  Slime damage will directly remove health, bypassing armor completely.
	 * <p>
	 * When armor is enabled, getting a key will also increase armor points. Armor can be disabled, in which case armor
	 * items and keys will increase HP like before.
	 */
	@SimpleMenuBuilder.InMenu(scope = "gamemod", title = "Enable armor")
	@InConfigFile(key = "EnableArmor")
	public boolean bEnableArmor;

	@SimpleMenuBuilder.InMenu(scope = "gamemod", title = "Jump mode")
	@InConfigFile(key = "JumpMode")
	public JumpMode jumpMode;

	@SimpleMenuBuilder.InMenu(scope = "gamemod", title = "Projectile gravity")
	@InConfigFile(key = "ProjectileGravity")
	public ProjectileGravity projectileGravity;

	public enum LSPKeys implements KeyType {
		Weapon_1,
		Weapon_2,
		Weapon_3,
		Last_Used_Weapon,
		Map_Follow_Mode,
		Toggle_Crosshair,
		AutoRun,
		Quickload,
		Quicksave,
		Show_Savemenu,
		Show_Loadmenu,
		Show_Sounds,
		Show_Options,
		Quit,
		Gamma,
		Crouch_toggle,
		Make_Screenshot;

		private int num = -1;

		public int getNum() { return num; }
		
		public String getName() { return name(); }
		
		public KeyType setNum(int num) { this.num = num; return this; }
	}

	public static final char[] defclassickeys = { 
			Keys.UP, // GameKeys.Move_Forward,
			Keys.DOWN, // GameKeys.Move_Backward,
			Keys.LEFT, // GameKeys.Turn_Left,
			Keys.RIGHT, // GameKeys.Turn_Right,
			Keys.BACKSPACE, // GameKeys.Turn_Around,
			Keys.ALT_LEFT, // GameKeys.Strafe,
			Keys.COMMA, // GameKeys.Strafe_Left,
			Keys.PERIOD, // GameKeys.Strafe_Right,
			Keys.A, // GameKeys.Jump,
			Keys.Z, // GameKeys.Crouch,
			0, //LSPKeys.Crouch_toggle,
			Keys.SPACE, // GameKeys.Use
			Keys.CONTROL_LEFT,// GameKeys.Weapon_Fire,
			Keys.ESCAPE,	// GameKeys.Menu_Toggle,
			Keys.NUM_1, //LSPKeys.Weapon_1
			Keys.NUM_2, //LSPKeys.Weapon_2
			Keys.NUM_3, //LSPKeys.Weapon_3
			Keys.SEMICOLON, //GameKeys.Previous_Weapon,
			Keys.APOSTROPHE, //GameKeys.Next_Weapon,
			0, //LSPKeys.Last_Used_Weapon,
			Keys.PAGE_UP, //GameKeys.Look_Up,
			Keys.PAGE_DOWN, //GameKeys.Look_Down,
			Keys.U, //GameKeys.Mouse_Aiming,
			Keys.SHIFT_LEFT, //GameKeys.Run
			KEY_CAPSLOCK, 		//AutoRun
			Keys.TAB, //GameKeys.Map_Toggle,
			Keys.EQUALS, //GameKeys.Enlarge_Screen,
			Keys.MINUS, //GameKeys.Shrink_Screen,
			Keys.GRAVE,		//Show_Console
			Keys.F, //LSPKeys.Map_Follow_Mode,
			Keys.I, //LSPKeys.Toggle_Crosshair,
			Keys.F9, //LSPKeys.Quickload,
			Keys.F6, //LSPKeys.Quicksave,
			Keys.F2, //LSPKeys.Show_Savemenu,
			Keys.F3, //LSPKeys.Show_Loadmenu,
			Keys.F4, //LSPKeys.Show_Sounds,
			Keys.F5, //LSPKeys.Show_Options,
			Keys.F10, //LSPKeys.Quit,
			Keys.F11, //LSPKeys.Gamma,
			Keys.F12, //LSPKeys.Make_Screenshot,
	};

	public static char[] defkeys = { 
			Keys.W, // GameKeys.Move_Forward,
			Keys.S, // GameKeys.Move_Backward,
			Keys.LEFT, // GameKeys.Turn_Left,
			Keys.RIGHT, // GameKeys.Turn_Right,
			Keys.BACKSPACE, // GameKeys.Turn_Around,
			Keys.ALT_LEFT, // GameKeys.Strafe,
			Keys.A, // GameKeys.Strafe_Left,
			Keys.D, // GameKeys.Strafe_Right,
			Keys.SPACE, // GameKeys.Jump,
			Keys.CONTROL_LEFT, // GameKeys.Crouch,
			0, //LSPKeys.Crouch_toggle,
			Keys.E, // GameKeys.Use
			0,// GameKeys.Weapon_Fire,
			Keys.ESCAPE,	// GameKeys.Menu_Toggle,
			Keys.NUM_1, //LSPKeys.Weapon_1
			Keys.NUM_2, //LSPKeys.Weapon_2
			Keys.NUM_3, //LSPKeys.Weapon_3
			Keys.SEMICOLON, //GameKeys.Previous_Weapon,
			Keys.APOSTROPHE, //GameKeys.Next_Weapon,
			Keys.Q, //LSPKeys.Last_Used_Weapon,
			Keys.PAGE_UP, //GameKeys.Look_Up,
			Keys.PAGE_DOWN, //GameKeys.Look_Down,
			Keys.U, //GameKeys.Mouse_Aiming,
			Keys.SHIFT_LEFT, //GameKeys.Run
			KEY_CAPSLOCK, 		//AutoRun
			Keys.TAB, //GameKeys.Map_Toggle,
			Keys.EQUALS, //GameKeys.Enlarge_Screen,
			Keys.MINUS, //GameKeys.Shrink_Screen,
			Keys.GRAVE,		//Show_Console
			Keys.F, //LSPKeys.Map_Follow_Mode,
			Keys.I, //LSPKeys.Toggle_Crosshair,
			Keys.F9, //LSPKeys.Quickload,
			Keys.F6, //LSPKeys.Quicksave,
			Keys.F2, //LSPKeys.Show_Savemenu,
			Keys.F3, //LSPKeys.Show_Loadmenu,
			Keys.F4, //LSPKeys.Show_Sounds,
			Keys.F5, //LSPKeys.Show_Options,
			Keys.F10, //LSPKeys.Quit,
			Keys.F11, //LSPKeys.Gamma,
			Keys.F12, //LSPKeys.Make_Screenshot,
	};

	/**
	 * This determines the behavior of jumping.
	 */
	public enum JumpMode {
		/**
		 * Jump like in the original DOS version of L7P.  When holding the jump key, the player lifts himself above the
		 * ground by a constant distance.  This doesn't enable the player to fly, though, because he will still fall
		 * into pits when jumping.  This is likely a bug in the DOS game.
		 */
		@SimpleMenuBuilder.EnumDescription("Classic")
		CLASSIC,

		/**
		 * Jump like in most games.
		 */
		@SimpleMenuBuilder.EnumDescription("Jump")
		JUMP,

		/**
		 * Jump like a real Xia in Wu Xia movies, who almost always knows Qing Gong.
		 * <p>
		 * The player can jump as high as desired by holding the jump key.
		 * The player falls at a very low speed, unless the player crunch,
		 * in which case the player experiences free-falling like in the JUMP mode.
		 * <p>
		 * This effectively enables the player to fly, but doesn't make him invincible.
		 */
		@SimpleMenuBuilder.EnumDescription("Qing Gong")
		QING_GONG;

		public static final JumpMode DEFAULT = QING_GONG;
	}

	/**
	 * This determines how the player's weapon projectiles are affected by gravity.
	 */
	public enum ProjectileGravity {
		/**
		 * Projectiles will bounce on the ground like bowling balls.
		 * <p>
		 * It's hard to believe, but this is how the DOS version behaves, especially if your CPU is too fast.
		 */
		@SimpleMenuBuilder.EnumDescription("Classic")
		CLASSIC,

		/**
		 * Projectiles will hover above the ground at a distance, and hardly bounces at all.
		 * <p>
		 * It is a more reasonable behavior for Anqi and Qigong projectiles.
		 * This behavior allows projectiles to get over small ledges and hit enemies more easily.
		 */
		@SimpleMenuBuilder.EnumDescription("Hover")
		HOVER,

		/**
		 * Projectiles fly in straight lines, ignoring gravity.
		 * <p>
		 * This simplifies the game mechanisms.  However, believe it or not, it is harder to hit enemies in this mode.
		 */
		@SimpleMenuBuilder.EnumDescription("No gravity")
		NO_GRAVITY;

		public static final ProjectileGravity DEFAULT = HOVER;
	}

	public Config(String path, String name) {
		super(path, name);
	}

	@Override
	public void SaveConfig(FileResource fil) {
		if(fil != null)
		{
			saveString(fil, "[Options]\r\n");	
			saveBoolean(fil, "AutoRun", gAutoRun);
			saveInteger(fil, "OverlayMap", gOverlayMap);
			saveBoolean(fil, "ShowMessages", gShowMessages);
			saveBoolean(fil, "Crosshair", gCrosshair);
			saveInteger(fil, "CrossSize", gCrossSize);
			saveInteger(fil, "ShowStat", gShowStat);
			saveInteger(fil, "HUDSize", gHUDSize);
			saveInteger(fil, "showMapInfo", showMapInfo);
			saveBoolean(fil, "HeadBob", bHeadBob);
			saveBoolean(fil, "ProjectileSpam", bOriginal);
			saveBoolean(fil, "ShowExits", bShowExit);
			saveAnnotatedFields(fil);
		}
	}

	void saveAnnotatedFields(FileResource fil) {
		for (Field field : Config.class.getDeclaredFields()) {
			for (InConfigFile anno : field.getAnnotationsByType(InConfigFile.class)) {
				String key = anno.key();
				if (field.getType().equals(boolean.class)) {
					boolean value;
					try {
						value = field.getBoolean(this);
					} catch (IllegalAccessException e) {
						throw new RuntimeException("Cannot read boolean field", e);
					}
					saveBoolean(fil, key, value);
				} else if (field.getType().isEnum()) {
					Enum<?> value;
					try {
						value = (Enum<?>)field.get(this);
					} catch (IllegalAccessException e) {
						throw new RuntimeException("Cannot read enum field", e);
					}
					int ordinal = value.ordinal();
					saveInteger(fil, key, ordinal);
				}
			}
		}
	}

	@Override
	public boolean InitConfig(boolean isDefault) {
		gAutoRun = false;
		gOverlayMap = 2;
		gShowMessages = true;
		gCrosshair = true;
		gCrossSize = 65536;
		gShowStat = 0;
		gHUDSize = 65536;
		showMapInfo = 1;
		bHeadBob = true;
		bShowExit = false;
		bRevisedAnqi2 = true;
		bRevisedAnqi3 = true;
		bRevisedAnqi4 = true;
		bRevisedQigong4 = true;
		bRevisedQigong5 = true;
		bRevisedQigong6 = true;
		bEnableArmor = true;
		jumpMode = JumpMode.DEFAULT;
		projectileGravity = ProjectileGravity.DEFAULT;

		if (!isDefault) {
			LoadCommon(defkeys, defclassickeys);
			if (set("Options")) {
				gAutoRun = GetKeyInt("AutoRun") == 1;
				int value = GetKeyInt("OverlayMap");
				if(value != -1) gOverlayMap = value;
				gShowMessages = GetKeyInt("ShowMessages") == 1;
				gCrosshair = GetKeyInt("Crosshair") == 1;
				value = GetKeyInt("CrossSize");
				if(value != -1) gCrossSize = BClipLow(value, 16384);
				value = GetKeyInt("ShowStat");
				if(value != -1) gShowStat = value;
				value = GetKeyInt("HUDSize");
				if(value != -1) gHUDSize = BClipLow(value, 16384);
				value = GetKeyInt("showMapInfo");
				if(value != -1) showMapInfo = value;
				bHeadBob = GetKeyInt("HeadBob") == 1;
				value = GetKeyInt("ProjectileSpam");
				if(value != -1) bOriginal = value == 1;
				value = GetKeyInt("ShowExits");
				if(value != -1) bShowExit = value == 1;
				loadAnnotatedFields();
			}
			close();
		} else {
			Console.Println("Config file not found, using default settings", OSDTEXT_YELLOW);

			for (int i = 0; i < keymap.length; i++)
				primarykeys[i] = defkeys[i];

			mousekeys[GameKeys.Weapon_Fire.getNum()] = MOUSE_LBUTTON;
			mousekeys[GameKeys.Open.getNum()] = MOUSE_RBUTTON;
			mousekeys[GameKeys.Next_Weapon.getNum()] = MOUSE_WHELLUP;
			mousekeys[GameKeys.Previous_Weapon.getNum()] = MOUSE_WHELLDN;
		}

		boolean mouseset = true;
		for (int i = 0; i < mousekeys.length; i++) {
			if (mousekeys[i] != 0) {
				mouseset = false;
				break;
			}
		}
		if (mouseset) {
			mousekeys[GameKeys.Weapon_Fire.getNum()] = MOUSE_LBUTTON;
			mousekeys[GameKeys.Open.getNum()] = MOUSE_RBUTTON;
			mousekeys[GameKeys.Next_Weapon.getNum()] = MOUSE_WHELLUP;
			mousekeys[GameKeys.Previous_Weapon.getNum()] = MOUSE_WHELLDN;
		}

		musicType = BClipRange(musicType, 0, 1);
		return true;
	}

	private void loadAnnotatedFields() {
		for (Field field : Config.class.getDeclaredFields()) {
			for (InConfigFile anno : field.getAnnotationsByType(InConfigFile.class)) {
				String key = anno.key();
				Class<?> fieldType = field.getType();
				if (fieldType.equals(boolean.class)) {
					int configValue = GetKeyInt(key);
					if(configValue != -1) {
						boolean fieldValue = configValue == 1;
						try {
							field.setBoolean(this, fieldValue);
						} catch (IllegalAccessException e) {
							throw new RuntimeException("Cannot write boolean field", e);
						}
					}
				} else if (fieldType.isEnum()) {
					int configValue = GetKeyInt(key);
					Object[] constants = fieldType.getEnumConstants();
					if (0 <= configValue && configValue < constants.length) {
						Object fieldValue = constants[configValue];
						try {
							field.set(this, fieldValue);
						} catch (IllegalAccessException e) {
							throw new RuntimeException("Cannot write enum field", e);
						}
					} // otherwise skip the assignment to use the default.
				}
			}
		}
	}

	@Override
	public KeyType[] getKeyMap() {
		KeyType[] keymap = { 
				GameKeys.Move_Forward, 
				GameKeys.Move_Backward, 
				GameKeys.Turn_Left, 
				GameKeys.Turn_Right,
				GameKeys.Turn_Around, 
				GameKeys.Strafe, 
				GameKeys.Strafe_Left, 
				GameKeys.Strafe_Right, 
				GameKeys.Jump,
				GameKeys.Crouch,
				LSPKeys.Crouch_toggle,
				GameKeys.Open, 
				GameKeys.Weapon_Fire, 
				GameKeys.Menu_Toggle, 
				LSPKeys.Weapon_1,
				LSPKeys.Weapon_2,
				LSPKeys.Weapon_3,
				GameKeys.Previous_Weapon,
				GameKeys.Next_Weapon,
				LSPKeys.Last_Used_Weapon,
				GameKeys.Look_Up,
				GameKeys.Look_Down,
				GameKeys.Mouse_Aiming,
				GameKeys.Run,
				LSPKeys.AutoRun,
				GameKeys.Map_Toggle,
				GameKeys.Enlarge_Screen,
				GameKeys.Shrink_Screen,
				GameKeys.Show_Console,
				
				LSPKeys.Map_Follow_Mode,
				LSPKeys.Toggle_Crosshair,
				LSPKeys.Quickload,
				LSPKeys.Quicksave,
				LSPKeys.Show_Savemenu,
				LSPKeys.Show_Loadmenu,
				LSPKeys.Show_Sounds,
				LSPKeys.Show_Options,
				LSPKeys.Quit,
				LSPKeys.Gamma,
				LSPKeys.Make_Screenshot,
			};
		return keymap;
	}

}
