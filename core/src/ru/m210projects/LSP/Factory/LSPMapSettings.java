// This file is part of LSPGDX.
// Copyright (C) 2021  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Factory;

import static ru.m210projects.Build.Engine.sector;
import static ru.m210projects.Build.Engine.totalclock;
import static ru.m210projects.Build.Engine.wall;
import static ru.m210projects.Build.Net.Mmulti.myconnectindex;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.Main.cfg;

import com.badlogic.gdx.Game;
import ru.m210projects.Build.Gameutils;
import ru.m210projects.Build.Render.DefaultMapSettings;
import ru.m210projects.Build.Types.WALL;

public class LSPMapSettings extends DefaultMapSettings {

	public static final int SECTOR_COLORS[] = {
			0x70, // light green
			0xa0, // light yellow
			0xc3, // light blue
			0xae, // light red
			0xd6, // light orange
			0xfa, // light pink
			0xb3, // medium red
			0xfb, // medium pink
			0x78, // dark green
			0xa4, // dark yelllow
			0xca, // dark blue
			0xda, // dark orange
			0xfc, // dark pink
	};

	public static int colorForHitag(int hitag) {
		if (hitag <= 0) {
			throw new IllegalArgumentException("hitag must be positive: " + hitag);
		}

		if (hitag - 1 >= SECTOR_COLORS.length) {
			return SECTOR_COLORS[SECTOR_COLORS.length - 1];
		} else {
			return SECTOR_COLORS[hitag - 1];
		}
	}

	public static int flashingColor(int speed, int color1, int color2) {
		return (totalclock / speed) % 2 == 0 ? color1 : color2;
	}

	@Override
	public int getWallColor(int w, int i) {
		WALL wal = wall[w];

		if (showSectorInfo) {
			int[] wallNums = {w, wal.nextwall};

			for (var wallNum : wallNums) {
				if (Gameutils.isValidWall(wallNum)) {
					WALL theWall = wall[wallNum];
					switch (theWall.lotag) {
						case 2 -> {
							return flashingColor(8, colorForHitag(theWall.hitag), 0x0);
						}
					}
				}
			}

			int[] secs = {i, wal.nextsector};

			for (int sec : secs) {
				if (Gameutils.isValidSector(sec)) {
					short lo = sector[sec].lotag;
					short hi = sector[sec].hitag;

					boolean special = false;
					int color = 0;

					switch (lo) {
						case 0 -> {
							if (hi != 0) {
								special = true;
								color = flashingColor(16, colorForHitag(hi), 0x0);
							}
						}
						case 1 -> {
							special = true;
							color = flashingColor(16, colorForHitag(hi), 0x0);
						}
						case 2 -> {
							if (hi != 0) {
								special = true;
								color = flashingColor(8, colorForHitag(hi), 0x0);
							}
						}
						case 6, 7, 8, 20, 21, 13, 16 -> {
							if (hi != 0) {
								special = true;
								color = colorForHitag(hi);
							}
						}
					}

					if (special) {
						//System.out.format("Sector %d lotag %d hitag %d%n", sec, sector[sec].lotag, sector[sec].hitag);
						return color;
					}
				}
			}
		}

		if (Gameutils.isValidSector(wal.nextsector)) // red wall
			return showSectorInfo ? 0x68 : 175; // When showing sector, we make internal walls grey instead.

		int walcol = 4;
		switch (sector[i].lotag) {
		case 97:
		case 98:
		case 99:
			if (!cfg.bShowExit)
				return -1;

			walcol = 120;
			if (sector[i].lotag == 99) // right exit
				walcol += (totalclock & 10);
			break;
		}
		return walcol; // white wall
	}

	@Override
	public boolean isShowRedWalls() {
		return true;
	}

	@Override
	public boolean isSpriteVisible(MapView view, int index) {
		return showSprites;
	}

	@Override
	public int getPlayerPicnum(int player) {
		return -1;
	}

	@Override
	public int getPlayerSprite(int player) {
		return gPlayer[player].nSprite;
	}

	@Override
	public boolean isFullMap() {
		return showFullMap;
	}

	@Override
	public boolean isScrollMode() {
		return followmode;
	}

	@Override
	public int getViewPlayer() {
		return myconnectindex;
	}
}
