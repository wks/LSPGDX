package ru.m210projects.LSP.Menus;

import ru.m210projects.Build.Pattern.BuildFont;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.LSP.Main;

import java.lang.annotation.*;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class SimpleMenuBuilder {
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface InMenu {
        String scope();
        String title();
    }

    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface EnumDescription {
        String value();
    }

    protected final Main app;
    protected final BuildMenu menu;

    public int width = 280;
    public int pos = 10;
    public int vDist = 12;
    public int left = 25;
    private boolean firstItem = true;

    public BuildFont getFont() {
        return app.getFont(0);
    }

    public SimpleMenuBuilder(Main app, BuildMenu menu) {
        this.app = app;
        this.menu = menu;
    }

    public void forObjectFields(Object object, String scope) {
        Class<?> klass = object.getClass();
        for (Field field : klass.getDeclaredFields()) {
            for (InMenu anno : field.getAnnotationsByType(InMenu.class)) {
                // Each field can have at most one `InMenu` annotation.  If we get here, `anno` must be that annotation.
                String itemScope = anno.scope();
                String title = anno.title();

                if (itemScope.equals(scope)) {
                    Class<?> fieldType = field.getType();
                    if (fieldType.equals(boolean.class)) {
                        forBooleanField(title, object, field);
                    } else if (fieldType.isEnum()) {
                        forEnumField(title, object, field, fieldType);
                    }
                }
            }
        }
    }

    public void forBooleanField(String title, Object object, Field field) {
        int myPos = getPos();

        boolean fieldValue;
        try {
            fieldValue = field.getBoolean(object);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Cannot load value.", e);
        }

        MenuSwitch result = new MenuSwitch(title, getFont(), left, myPos, width, fieldValue, new MenuProc() {
            @Override
            public void run(MenuHandler handler, MenuItem pItem ) {
                MenuSwitch sw = (MenuSwitch) pItem;
                try {
                    field.setBoolean(object, sw.value);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException("Cannot store value.", e);
                }
            }
        }, null, null);

        addItem(result);
    }

    public void forEnumField(String title, Object object, Field field, Class<?> fieldType) {
        final int myPos = getPos();

        final Enum fieldValue;
        try {
            fieldValue = (Enum)field.get(object);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Cannot load value.", e);
        }

        var constants = new ArrayList<Enum>();
        var strings = new ArrayList<String>();

        for (Field constantField : fieldType.getDeclaredFields()) {
            if (constantField.isEnumConstant()) {
                Enum constant = null;
                try {
                    constant = (Enum)constantField.get(null);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException("Cannot load enum constant field.", e);
                }
                constants.add(constant);

                String string = null;
                for (EnumDescription anno : constantField.getAnnotationsByType(EnumDescription.class)) {
                    string = anno.value();
                }
                if (string == null) {
                    string = constant.toString();
                }
                strings.add(string);
            }
        }

        MenuConteiner result = new MenuConteiner(title, getFont(), left, myPos, width, strings.toArray(String[]::new), fieldValue.ordinal(), new MenuProc() {
            @Override
            public void run(MenuHandler handler, MenuItem pItem) {
                MenuConteiner item = (MenuConteiner) pItem;
                try {
                    field.set(object, constants.get(item.num));
                } catch (IllegalAccessException e) {
                    throw new RuntimeException("Cannot store value.", e);
                }
            }
        });

        addItem(result);
    }

    protected int getPos() {
        final int myPos = pos;
        pos += vDist;
        return myPos;
    }

    protected void addItem(MenuItem item) {
        menu.addItem(item, firstItem);
        firstItem = false;
    }
}
