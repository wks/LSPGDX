// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Net.Mmulti.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.LSP.Quotes.viewSetMessage;
import static ru.m210projects.LSP.Main.*;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.Sounds.*;
import static ru.m210projects.LSP.Weapons.*;
import static ru.m210projects.LSP.Enemies.*;
import static ru.m210projects.LSP.View.*;

import ru.m210projects.Build.Types.SPRITE;
import ru.m210projects.LSP.Types.PlayerStruct;

import java.util.ArrayList;
import java.util.Optional;

public class Sprites {
	// Hit definitions
	public static final int kHitTypeMask	= 0xE000;
	public static final int kHitIndexMask	= 0x1FFF;
	public static final int kHitSector		= 0x4000;
	public static final int kHitWall		= 0x8000;
	public static final int kHitSprite		= 0xC000;

	public static boolean isQigongProjectile(int picnum) {
		if (QIGONG5_PROJ <= picnum && picnum <= QIGONG5_PROJ + 8) {
			return true;
		}
		return switch (picnum) {
			case QIGONG1_PROJ, QIGONG2_PROJ, QIGONG3_PROJ, QIGONG4_PROJ, QIGONG6_PROJ -> true;
			default -> false;
		};
	}

	public static boolean isAnqiProjectile(int picnum) {
		return switch (picnum) {
			case ANQI1_PROJ, ANQI2_PROJ, ANQI3_PROJ, ANQI4_PROJ, ANQI5_PROJ, ANQI6_PROJ -> true;
			default -> false;
		};
	}

	public static boolean isDudeProjectile(int picnum) {
		return switch (picnum) {
			case GREENDUDE_PROJ, REDDUDE_PROJ, PURPLEDUDE_PROJ, YELLOWDUDE_PROJ -> true;
			default -> false;
		};
	}

	public static boolean isStatueProjectile(int picnum) {
		return switch (picnum) {
			case QIGONG1_PROJ, SKULL_PROJ -> true;
			default -> false;
		};
	}

	public static boolean isPlayerProjectile(SPRITE spr) {
		if (spr.picnum == QIGONG1_PROJ) {
			return spr.owner >= 4096;
		}
		return isAnqiProjectile(spr.picnum) || isQigongProjectile(spr.picnum);
	}

	public static boolean isEnemyProjectile(SPRITE spr) {
		if (spr.picnum == QIGONG1_PROJ) {
			// The buddha statue shoots this projectile, too.
			return spr.owner < 4096;
		}
		return isStatueProjectile(spr.picnum) || isDudeProjectile(spr.picnum);
	}

	/** Return true if the sprite is a destroyable environment sprite, such as torch, lantern, tree, window, furniture, etc. */
	public static boolean isBreakable(int picnum) {
		return BREAKABLE_BEGIN <= picnum && picnum < BREAKABLE_END;
	}

	/** Break the breakable sprite and make it un-hittable. */
	public static void breakBreakable(SPRITE spr) {
		spr.picnum += BREAKABLE_TO_BROKEN;
		spr.cstat &= ~257;
	}

	public static void operatesprite(int dasprite) {
		SPRITE spr = sprite[dasprite];
		if (spr.lotag == 2)
			shoot(dasprite, spr.x, spr.y, spr.z, spr.ang, 100, spr.sectnum, 9);
	}

	public static int changeammo(int plr, int nWeapon, int value) {
		if (gPlayer[plr].nPlayerStatus == 5) {
			gPlayer[plr].nAmmo[nWeapon] = 160;
			return 0;
		}
		if (value <= 0) {
			gPlayer[plr].nAmmo[nWeapon] += value;
			if (gPlayer[plr].nAmmo[nWeapon] < 0)
				gPlayer[plr].nAmmo[nWeapon] = 0;
			return 0;
		}
		if (gPlayer[plr].nAmmo[nWeapon] >= 160)
			return 0;
		gPlayer[plr].nAmmo[nWeapon] += value;
		if (gPlayer[plr].nAmmo[nWeapon] > 160)
			gPlayer[plr].nAmmo[nWeapon] = 160;
		TintPalette(0, 16, 0);
		return 1;
	}

	public static int changemanna(int a1, int a2) {
		if (gPlayer[a1].nPlayerStatus == 5) {
			gPlayer[a1].nMana = 160;
			return 0;
		}
		if (a2 <= 0) {
			gPlayer[a1].nMana += a2;
			if (gPlayer[a1].nMana < 0)
				gPlayer[a1].nMana = 0;
			return 0;
		}
		if (gPlayer[a1].nMana >= 160)
			return 0;
		gPlayer[a1].nMana += a2;
		if (gPlayer[a1].nMana > 160)
			gPlayer[a1].nMana = 160;

		TintPalette(0, 4, 16);
		return 1;
	}

	public enum ChangeHealthKind {
		HEAL, GET_ARMOR, HIT_DAMAGE, EXPLOSION_DAMAGE, SLIME_DAMAGE;
	}

	public static boolean changehealth(int snum, int deltahealth, ChangeHealthKind kind) {
		if (gPlayer[snum].nPlayerStatus == 5) {
			gPlayer[snum].nHealth = MAXHEALTH;
			gPlayer[snum].nArmor = MAXARMOR;
			return false;
		}

		if (!cfg.bEnableArmor && kind == ChangeHealthKind.GET_ARMOR) {
			// If configured to not use armor, armor items will simply heal the player.
			kind = ChangeHealthKind.HEAL;
		}

		boolean out = false;
		switch (kind) {
			case HEAL: {
				if (deltahealth <= 0) {
					throw new IllegalArgumentException("Healing but deltahealth <= 0: " + deltahealth);
				}
				if (gPlayer[snum].nHealth < MAXHEALTH) {
					gPlayer[snum].nHealth += deltahealth;
					if (gPlayer[snum].nHealth > MAXHEALTH)
						gPlayer[snum].nHealth = MAXHEALTH;
					out = true;
				}
				break;
			}
			case GET_ARMOR: {
				if (deltahealth <= 0) {
					throw new IllegalArgumentException("Getting armor but deltahealth <= 0: " + deltahealth);
				}
				if (gPlayer[snum].nArmor < MAXARMOR) {
					gPlayer[snum].nArmor += deltahealth;
					if (gPlayer[snum].nArmor > MAXARMOR)
						gPlayer[snum].nArmor = MAXARMOR;
					out = true;
				}
				break;
			}
			case HIT_DAMAGE:
			case EXPLOSION_DAMAGE:
			case SLIME_DAMAGE: {
				if (deltahealth >= 0) {
					throw new IllegalArgumentException("Damaged but deltahealth >= 0: " + deltahealth);
				}

				if (gPlayer[snum].nHealth > 0)
					TintPalette(16, 0, 0);

				final int actualDamage = kind == ChangeHealthKind.EXPLOSION_DAMAGE ?
						-deltahealth :
						switch (nDifficult) {
					case 1 -> -deltahealth / 2;
					case 3 -> -deltahealth * 2;
					default -> -deltahealth;
				};

				// Work out how much damage goes to health and how much goes to armor.
				int toHealth;
				int toArmor;
				if (kind == ChangeHealthKind.SLIME_DAMAGE) {
					toHealth = actualDamage;
					toArmor = 0;
				} else {
					// Split the damage.
					int splitToHealth = actualDamage / 2; // Health absorbs 1/2 of the total damage, rounded down.
					int splitToArmor = actualDamage - splitToHealth; // Armor absorbs the rest of the damage.

					// See if there is enough armor.
					if (gPlayer[snum].nArmor >= splitToArmor) {
						// If there is enough armor, split the damage as is.
						toHealth = splitToHealth;
						toArmor = splitToArmor;
					} else {
						// Otherwise, the armor wears out, and remaining damage goes to the health.
						toHealth = splitToHealth + splitToArmor - gPlayer[snum].nArmor;
						toArmor = gPlayer[snum].nArmor;
					}
				}

				gPlayer[snum].nHealth -= toHealth;
				gPlayer[snum].nArmor -= toArmor;
			}
		}

		if (gPlayer[snum].nHealth <= 0) {
			if ((engine.krand() & 1) != 0)
				playsound(9);
			else
				playsound(10);
			globalvisibility = 15;
			gPlayer[snum].nHealth = 0;
			return false;
		}

		if (gPlayer[snum].nHealth >= 25) {
			globalvisibility = 15;
			return out;
		}

		if (gPlayer[snum].nHealth < 8)
			globalvisibility = 9;
		else if (gPlayer[snum].nHealth < 18)
			globalvisibility = 11;
		else
			globalvisibility = 13;

		playsound(88);
		return out;
	}

	public static void checktouchsprite(int snum) {
		short sectnum = gPlayer[snum].sectnum;
		if ((sectnum < 0) || (sectnum >= numsectors)) {
			return;
		}

		short i = headspritesect[sectnum], nexti;
		while (i != -1) {
			nexti = nextspritesect[i];
			if ((Math.abs(gPlayer[snum].x - sprite[i].x) + Math.abs(gPlayer[snum].y - sprite[i].y) < 512)) {
				int height = (sprite[i].z >> 8) - (engine.getTile(sprite[i].picnum).getHeight() >> 1);
				if (Math.abs((gPlayer[snum].z >> 8) - height) <= 40) {
					switch (sprite[i].picnum) {
					case 601: // a paper umbrella
					case 602: // a straw shield
						if (changehealth(snum, sprite[i].picnum == 601 ? 10 : 30, ChangeHealthKind.GET_ARMOR)) {
							viewSetMessage("Picked up " + (sprite[i].picnum == 601 ? "a Parasol" : "a Shield")); // 傘/盾
							TintPalette(16, 16, 0);
							playsound(51);
							engine.deletesprite(i);
						}
						break;
					case 603: // A golden armor.
						if (changehealth(snum, 100, ChangeHealthKind.GET_ARMOR)) {
							// Note: The Chinese name “金縷衣” literally means "gold-thread clothing".
							// In an ancient poem of the same name, it refers to a kind of robe woven with gold threads
							// that is good-looking and valuable.
							// But the game item is an armor.  It is likely a kind of soft armor made with gold threads
							// or other metal.  It is usually worn under outer clothes to protect the wearer against
							// assassination.
							viewSetMessage("Picked up a Gold-thread Armor"); // 金縷衣
							TintPalette(16, 16, 0);
							playsound(51);
							playsound(51);
							playsound(51);
							engine.deletesprite(i);
						}
						break;
					case 604: // ancient Chinese coins. Each coin is round-shaped with a square hole on it.
						if (changeammo(snum, 1, 12) == 0)
							break;

						viewSetMessage("Picked up Ancient Coins"); // 古錢
						playsound(56);
						engine.deletesprite(i);
						break;
					case 605: // balls
						if (changeammo(snum, 2, 10) == 0)
							break;

						viewSetMessage("Picked up Iron Balls"); // 鐵球
						playsound(26);
						engine.deletesprite(i);
						break;
					case 606: // needles
						if (changeammo(snum, 3, 10) == 0)
							break;

						viewSetMessage("Picked up Sleeve Needles"); // 袖裡針
						playsound(27);
						engine.deletesprite(i);
						break;
					case 607: // cross-shaped darts, similar to Japanese shurikens
						if (changeammo(snum, 4, 8) == 0)
							break;

						viewSetMessage("Picked up Cross Darts"); // 十字鏢
						playsound(32);
						engine.deletesprite(i);
						break;
					case 608: // daggers
						if (changeammo(snum, 5, 6) == 0)
							break;

						viewSetMessage("Picked up Throwing Daggers"); // 飛刀
						playsound(35);
						engine.deletesprite(i);
						break;
					case 609: // darts with ribbon tied to the hole on the rear, similar to Japanese kunais
						if (changeammo(snum, 6, 3) == 0)
							break;

						viewSetMessage("Picked up Throwing Darts"); // 飛鏢
						playsound(90);
						engine.deletesprite(i);
						break;
					case 700: // Big root with red ribbon on it
						if (changehealth(snum, 50, ChangeHealthKind.HEAL)) {
							viewSetMessage("Picked up Big Ginseng"); // 大人參
							TintPalette(16, 16, 0);
							playsound(51);
							engine.deletesprite(i);
						}
					case 701: // Small root with red ribbon on it
						if (changehealth(snum, 32, ChangeHealthKind.HEAL)) {
							viewSetMessage("Picked up Small Ginseng"); // 小人參
							TintPalette(16, 16, 0);
							playsound(51);
							engine.deletesprite(i);
						}
						break;
					case 702: // black pills on a red sheet
						if (changehealth(snum, 16, ChangeHealthKind.HEAL)) {
							viewSetMessage("Picked up Greater Recovery Pills"); // 大還丹
							TintPalette(16, 16, 0);
							playsound(50);
							engine.deletesprite(i);
						}
						break;
					case 703: // herbs wrapped in a bundle
						if (changehealth(snum, 8, ChangeHealthKind.HEAL)) {
							viewSetMessage("Picked up Medicinal Herbs"); // 藥草
							TintPalette(16, 16, 0);
							playsound(50);
							playsound(50);
							playsound(50);
							engine.deletesprite(i);
						}
						break;
					case 705: // flower. Saussurea, a.k.a. Snow Lotus.
						if (changemanna(snum, 25) != 0) {
							viewSetMessage("Picked up a Snow Lotus of Tianshan"); // 天山雪蓮
							playsound(29);
							engine.deletesprite(i);
						}
						break;
					case 706: // ganoderma, a kind of fungus, looks like a big mushroom
						if (changemanna(snum, 12) != 0) {
							viewSetMessage("Picked up a Ganoderma"); // 靈芝
							playsound(29);
							engine.deletesprite(i);
						}
						break;
					case 707: // a blue bottle. The medicine is supposed to help the user channel Qi Gong.
						if (changemanna(snum, 5) != 0) {
							viewSetMessage("Picked up a Bottle of Channeling Powder"); // 運功散
							playsound(29);
							engine.deletesprite(i);
						}
						break;
					case 710: // blue book
						if (gPlayer[snum].nAmmo[8] == 0) {
							TintPalette(0, 4, 16);
							viewSetMessage("Picked up the Book of Wild Fire Roaring Violently"); // 野火狂嘯
							changemanna(snum, 25);
							playsound(58);
							engine.deletesprite(i);
							gPlayer[snum].nAmmo[8] = 1;
						}
						break;
					case 711: // brown book
						if (gPlayer[snum].nAmmo[9] == 0) {
							TintPalette(0, 4, 16);
							viewSetMessage("Picked up the Book of Dragons Flying to The Seas"); // 龍騰四海
							changemanna(snum, 35);
							playsound(59);
							engine.deletesprite(i);
							gPlayer[snum].nAmmo[9] = 1;
						}
						break;
					case 712: // stone with text carved on it
						if (gPlayer[snum].nAmmo[10] == 0) {
							TintPalette(0, 4, 16);
							viewSetMessage("Picked up the Book of Thunder Weighing a Thousand Jun"); // 雷霆千鈞
							changemanna(snum, 50);
							playsound(60);
							engine.deletesprite(i);
							gPlayer[snum].nAmmo[10] = 1;
						}
						break;
					case 713: // wooden book
						if (gPlayer[snum].nAmmo[11] == 0) {
							TintPalette(0, 4, 16);
							viewSetMessage("Picked up the Book of Reversing The Heaven and The Earth"); // 逆轉乾坤
							changemanna(snum, 75);
							playsound(61);
							engine.deletesprite(i);
							gPlayer[snum].nAmmo[11] = 1;
						}
						break;
					case 714: // papirus
						if (gPlayer[snum].nAmmo[12] == 0) {
							TintPalette(0, 4, 16);
							viewSetMessage("Picked up the Book of Furious Moon Slashing Through Stars"); // 怒月劈星
							changemanna(snum, 100);
							playsound(62);
							engine.deletesprite(i);
							gPlayer[snum].nAmmo[12] = 1;
						}
						break;
					case 720: // gold key
					case 721: // bronze key
					case 722: // silver key
					case 723: // grey key
					case 724: // green key
					case 725: // red key
						viewSetMessage("Picked up a Key");
						TintPalette(0, 16, 0);
						if (cfg.bEnableArmor) {
							// When the armor system is enabled, keys will give the player both health and armor.
							changehealth(snum, engine.krand() % 60 + 36, ChangeHealthKind.HEAL);
							changehealth(snum, engine.krand() % 60 + 36, ChangeHealthKind.GET_ARMOR);
						} else {
							changehealth(snum, engine.krand() % 100 + 60, ChangeHealthKind.HEAL);
						}
						changemanna(snum, engine.krand() % 100 + 60);
						playsound(50);
						playsound(29);
						playsound(50);
						playsound(29);
						engine.deletesprite(i);
						if (sprite[i].picnum == 725) {
							for (short sec = 0; sec < numsectors; sec++) {
								for (short spr = headspritesect[sec]; spr != -1; spr = nextspritesect[spr]) {
									switch (sprite[spr].picnum) {
									case 275:
									case 339:
									case BLUEDUDE:
									case GREENDUDE:
									case REDDUDE:
									case PURPLEDUDE:
									case YELLOWDUDE:
										PlayerStruct plr = gPlayer[snum];
										SPRITE pspr = sprite[spr];

										int sin = sintable[(int) plr.ang & 2047];
										int cos = sintable[(int) (plr.ang + 512) & 2047];

										if (plr.sectnum >= 0
												&& (cos * (pspr.x - plr.x)) + (sin * (pspr.y - plr.y)) >= 0) {
											if (engine.cansee(plr.x, plr.y, plr.z, plr.sectnum, pspr.x, pspr.y,
													pspr.z - (engine.getTile(pspr.picnum).getHeight() << 7), pspr.sectnum)) {
												if (sprite[spr].picnum != 275 && sprite[spr].picnum != 339)
													enemydie(spr);
												if (spr % 5 == 1)
													playsound((engine.krand() & 7) + 12, spr);
											}
										}
										break;
									}
								}
							}
							changemanna(snum, -200);
						}
						break;
					case 727: // gold coin

						break;
					}
				}
			}
			i = nexti;
		}
	}

	private static boolean isAffectedByGravity(SPRITE spr) {
		if (cfg.projectileGravity == Config.ProjectileGravity.NO_GRAVITY) {
			return false;
		} else if (cfg.bRevisedAnqi2 && spr.picnum == ANQI2_PROJ) {
			return false;
		} else if (spr.picnum == QIGONG4_PROJ && spr.hitag == QIGONG4_HITAG_BIG) {
			// Big lightning balls are not affected by gravity.
			// Only creatable when cfg.bRevisedQigong4 is true.
			return false;
		} else if (spr.picnum != ANQI1_PROJ && spr.picnum != ANQI2_PROJ && spr.picnum != ANQI4_PROJ && spr.picnum != ANQI6_PROJ
				&& spr.picnum != QIGONG1_PROJ && spr.picnum != QIGONG2_PROJ && spr.picnum != QIGONG3_PROJ && spr.picnum != QIGONG4_PROJ) {
			return false;
		} else {
			return true;
		}
	}

	private static void applyProjectileGravity(SPRITE spr) {
		// Increment of z-velocity every tick.
		final int PROJECTILE_GRAVITY;
		// How high projectiles should hover above the ground.
		final int PROJECTILE_HOVER_ALTITUDE;
		// How much the z-velocity decay when hit the floor or ceiling.
		final int PROJECTILE_ZVEL_DECAY;

		switch (cfg.projectileGravity) {
			case CLASSIC -> {
				// In the DOS version of L7P, projectiles are attracted to the floor every frame.
				// This means, the faster your CPU is, the stronger the gravity is. :)
				// This number is rather big.  It behaves like when the CPU is fast.
				PROJECTILE_GRAVITY = 32;
				// This is rather low.  Projectiles will roll on the floor like bowling balls.
				PROJECTILE_HOVER_ALTITUDE = 512;
				// Quite bouncy.
				PROJECTILE_ZVEL_DECAY = 1;
			}
			case HOVER -> {
				// We choose a reasonable value that we feel comfortable.
				// A good value should make the projectiles hover at a distance above the floor,
				// and not feel very heavy.
				PROJECTILE_GRAVITY = 8;
				// A good value should make the whole (or at least the major parts of the) projectile visible
				// above ground (i.e. projectiles should not be buried into the floor).
				//
				// Note that the DOS version of L7P doesn't have any (horizontal or vertical)
				// auto-aiming mechanisms, and it doesn't support mouse-look.  The "+" and "-" keys in the
				// numerical keyboard only looks up and down temporarily.  All of these make it very hard for
				// the player to aim up and down.
				// Although Wuxia fans would intuitively expect fast-moving Anqi and the blasts
				// of Qigong to travel in straight lines, the fact that projectiles follow the geographic
				// contour may make it easier for player to hit enemies above or below small ledges.
				//
				// FIXME: Hovering projectiles are almost impossible to hit enemies in water.
				// It wasn't a problem in the DOS version because enemies seem not to be affected by water at all!
				// Enemies walk on water like walking on land. (Wow! Nice Qinggong!)
				PROJECTILE_HOVER_ALTITUDE = 0x1400;
				// With this number, projectiles hardly bounces at all.
				PROJECTILE_ZVEL_DECAY = 4;
			}
			default -> throw new IllegalStateException("Unexpected value: " + cfg.projectileGravity);
		}

		spr.z += spr.zvel;
		spr.zvel += PROJECTILE_GRAVITY * TICSPERFRAME;
		int cz = sector[spr.sectnum].ceilingz + 512;
		if (cz > spr.z) {
			spr.z = cz;
			spr.zvel = (short) -(spr.zvel >> PROJECTILE_ZVEL_DECAY);
		}

		int fz = sector[spr.sectnum].floorz - PROJECTILE_HOVER_ALTITUDE;
		if (fz < spr.z) {
			spr.z = fz;
			spr.zvel = (short) -(spr.zvel >> PROJECTILE_ZVEL_DECAY);
		}
	}

	private static int makeProjectileExplosion(SPRITE spr) {
		int blowspr = -1;
		short picnum = -1;
		byte shade = -1;
		short size = -1;
		short duration = -1;

		switch (spr.picnum) {
			case ANQI1_PROJ, ANQI2_PROJ, ANQI3_PROJ, ANQI4_PROJ, ANQI5_PROJ, ANQI6_PROJ -> {
				picnum = 1364;
				shade = -23;
				size = 48;
				duration = 63;
			}
			case QIGONG1_PROJ -> {
				picnum = 1358;
				shade = -24;
				size = 128;
				duration = 64;
			}
			case QIGONG2_PROJ -> {
				picnum = 1345;
				shade = -24;
				size = 72;
				duration = 64;
			}
			case QIGONG3_PROJ -> {
				picnum = 1354;
				shade = -24;
				size = 72;
				duration = 80;
			}
			case QIGONG4_PROJ -> {
				picnum = 1369;
				shade = -24;
				switch (spr.hitag) {
					default -> {
						size = 64;
						duration = 100;
					}
					case QIGONG4_HITAG_BIG -> {
						size = 80;
						duration = 125;
					}
					case QIGONG4_HITAG_SMALL -> {
						size = 48;
						duration = 60;
					}
				}
			}
			case QIGONG5_PROJ,
					QIGONG5_PROJ + 1,
					QIGONG5_PROJ + 2,
					QIGONG5_PROJ + 3,
					QIGONG5_PROJ + 4,
					QIGONG5_PROJ + 5,
					QIGONG5_PROJ + 6,
					QIGONG5_PROJ + 7,
					QIGONG5_PROJ + 8 -> {
				picnum = (short) ((engine.krand() & 3) + 1374);
				shade = -24;
				size = 48;
				duration = (short) ((engine.krand() & 0x7F) + 100);
			}
			case QIGONG6_PROJ -> {
				picnum = 1328;
				shade = -24;
				size = 72;
				duration = (short) ((engine.krand() & 0x3F) + 12);
			}
			case SKULL_PROJ, PURPLEDUDE_PROJ -> {
				picnum = 2356;
				shade = -23;
				size = 32;
				duration = 63;
			}
			case GREENDUDE_PROJ -> {
				picnum = 1844;
				shade = -23;
				size = 32;
				duration = 63;
			}
			case REDDUDE_PROJ -> { // gray coin
				picnum = 1364;
				shade = -23;
				size = 32;
				duration = 48;
			}
			case YELLOWDUDE_PROJ -> {
				picnum = 2612;
				shade = -23;
				size = 24;
				duration = 63;
			}
			default -> throw new RuntimeException(String.format("Unknown projectile %d", spr.picnum));
		}

		blowspr = engine.insertsprite(spr.sectnum, EXPLOSION);
		sprite[blowspr].x = spr.x;
		sprite[blowspr].y = spr.y;
		sprite[blowspr].z = spr.z;
		sprite[blowspr].cstat = 128;
		sprite[blowspr].picnum = picnum;
		sprite[blowspr].shade = shade;
		sprite[blowspr].xrepeat = size;
		sprite[blowspr].yrepeat = size;
		sprite[blowspr].ang = 0;
		sprite[blowspr].xvel = 0;
		sprite[blowspr].yvel = 0;
		sprite[blowspr].zvel = 0;
		sprite[blowspr].owner = spr.owner;
		sprite[blowspr].lotag = duration;

		return blowspr;
	}

	private static void playProjectileHitWallSound(short sprnum) {
		int picnum = sprite[sprnum].picnum;
		if (isAnqiProjectile(picnum)) {
			playsound(49, sprnum);
		} else if (isQigongProjectile(picnum)) {
			playsound(37 + (engine.krand() & 7), sprnum);
		} else {
			switch (sprite[sprnum].picnum) {
				case GREENDUDE_PROJ, SKULL_PROJ, PURPLEDUDE_PROJ -> playsound(39, sprnum);
			}
		}
	}

	private static void playEnemyHitSound(int projPicnum, int dudePicnum, int dudeSprIndex) {
		if (isAnqiProjectile(projPicnum)) {
			switch (dudePicnum) {
				case BLUEDUDE -> playsound(65, dudeSprIndex);
				case GREENDUDE -> playsound(66, dudeSprIndex);
				case REDDUDE -> playsound(70, dudeSprIndex);
				case PURPLEDUDE -> playsound(72, dudeSprIndex);
				case YELLOWDUDE -> playsound(68, dudeSprIndex);
			}
		} else if (isQigongProjectile(projPicnum)) {
			switch (dudePicnum) {
				case BLUEDUDE, YELLOWDUDE -> playsound(12, dudeSprIndex);
				case GREENDUDE -> playsound(68, dudeSprIndex);
				case REDDUDE -> playsound(70, dudeSprIndex);
				case PURPLEDUDE -> playsound(73, dudeSprIndex);
			}
		}
	}

	private static void playEnemyDeathSound(int projPicnum, int dudePicnum, int dudeSprIndex) {
		if (isAnqiProjectile(projPicnum)) {
			switch (dudePicnum) {
				case BLUEDUDE, REDDUDE -> playsound(15, dudeSprIndex);
				case GREENDUDE, PURPLEDUDE -> playsound(18, dudeSprIndex);
				case YELLOWDUDE -> playsound(11, dudeSprIndex);
			}
		} else if (isQigongProjectile(projPicnum)) {
			switch (dudePicnum) {
				case BLUEDUDE, REDDUDE -> playsound(11, dudeSprIndex);
				case GREENDUDE, PURPLEDUDE -> playsound(16, dudeSprIndex);
				case YELLOWDUDE -> playsound(19, dudeSprIndex);
			}
		}
	}

	private static void moveprojectiles() {
		var postponedJobs = new ArrayList<Runnable>();

		short nexti;
		for (short i = headspritestat[PROJECTILE]; i >= 0; i = nexti) {
			nexti = nextspritestat[i];

			SPRITE spr = sprite[i];
			game.pInt.setsprinterpolate(i, spr);

			// These are used for engine.movesprite
			int xvel, yvel, zvel;

			// Determine horizontal velocity
			if (!cfg.bRevisedQigong6 && spr.picnum == QIGONG6_PROJ) {
				spr.ang += 16;
				xvel = sintable[(spr.ang + 2560) & 0x7FF] >> 5;
				yvel = sintable[(spr.ang + 2048) & 0x7FF] >> 5;
			} else {
				xvel = spr.xvel;
				yvel = spr.yvel;
			}

			final boolean applyGravity = isAffectedByGravity(spr);

			// Determine vertical velocity for engine.movesprite
			zvel = applyGravity ? 0 : spr.zvel;

			if (applyGravity) {
				applyProjectileGravity(spr);
			}

			// Determine other properties before moving sprite.

			// Revised game: Some projectiles are larger than others.
			int clipdist = spr.clipdist << 2;
			if (cfg.bRevisedAnqi4 && spr.picnum == ANQI4_PROJ) {
				clipdist *= 2;
			}

			// Revised game: Some projectiles may pierce through enemies.
			final boolean pierce = cfg.bRevisedAnqi4 && spr.picnum == ANQI4_PROJ;

			// Workaround: Set all player to be un-hittable so that player projectiles can pass through players unharmed.
			if (isPlayerProjectile(spr)) {
				for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
					sprite[gPlayer[p].nSprite].cstat &= ~1;
				}
			}

			// Move the sprite and determine collision.
			clipmoveboxtracenum = 1;
			final int hitmove;
			if (pierce) {
				hitmove = pierceMoveSprite(i, xvel, yvel, zvel, clipdist, 512, 512, CLIPMASK0, TICSPERFRAME);
			} else {
				hitmove = engine.movesprite(i, xvel, yvel, zvel, clipdist, 512, 512, CLIPMASK0, TICSPERFRAME);
			}
			clipmoveboxtracenum = 3;

			// Restore the player's hittable flag.
			if (isPlayerProjectile(spr)) {
				for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
					sprite[gPlayer[p].nSprite].cstat |= 1;
				}
			}

			// Did it hit anything?
			if (hitmove != 0) {
				// Whatever the projectile hits, it creates an EXPLOSION sprite.
				// Note: If a dude projectile hits the player, the explosion may be adjusted to look better on screen.
				int blowspr = makeProjectileExplosion(spr);

				if (spr.picnum == QIGONG4_PROJ && spr.hitag == 1) {
					// If the big lightning ball hits anything,
					// it will explode into smaller lightning balls.

					// However, we are currently traversing the list of sprites.
					// We have to postpone the creation of sprites after we traversed all sprites.
					// We need to capture the field values, not the spr instance itself.
					final int x = spr.x;
					final int y = spr.y;
					final int z = spr.z;
					final short ang = spr.ang;
					final int horiz = 100;
					final short sectnum = spr.sectnum;
					final short owner = spr.owner;
					postponedJobs.add(() -> {
						Weapons.createQigong4Ring(true, x, y, z, ang, horiz, sectnum, owner);
					});
				}

				int nHitObject = hitmove & 0x0FFF;
				switch (hitmove & kHitTypeMask) {
				case kHitSector:
				case kHitWall:
					playProjectileHitWallSound(i);

					if (cfg.bRevisedAnqi2 && spr.picnum == ANQI2_PROJ) {
						if(spr.hitag > 0) {
							// In this mode, the ball shall bounce on walls like FREEZEBLAST in Duke3D.

							// hitag records the remaining times the ball can bounce.
							spr.hitag -= 1;
							if ((hitmove & kHitTypeMask) == kHitSector) {
								spr.zvel = (short) -spr.zvel;
							} else {
								int hitIndex = hitmove & kHitIndexMask;
								bounceFromWall(spr, hitIndex);
							}
							continue;
						}
					}

					if (!cfg.bRevisedQigong6 && spr.picnum == QIGONG6_PROJ) {
						if ((hitmove & kHitTypeMask) != kHitSector){
							// In the classic game, the "boomerang" simply turns by a fixed angle when hit.
							spr.ang = (short) ((spr.ang + 0x100) & 0x7FF);
							spr.zvel -= 2;
							continue;
						}
					}

					if (cfg.bRevisedQigong6 && spr.picnum == QIGONG6_PROJ) {
						// In this mode, the "boomerang" shall bounce from the wall and search for another target.
						// If no target is in sight, bounce like metal balls.
						if (spr.hitag > 0) {
							spr.hitag -= 1;

							if ((hitmove & kHitTypeMask) == kHitSector) {
								spr.zvel = (short) -spr.zvel;
								// Automatically search for a new target when hitting the floor or ceiling.
								qigong6FreeSearchNewTarget(spr, -1);
							} else {
								int hitIndex = hitmove & kHitIndexMask;
								// Bounce and search for a new target.
								qigong6BounceFromWall(spr, hitIndex);
							}
							continue;
						}
					}

					engine.deletesprite(i);
					continue;
				case kHitSprite:
					// If it hits a player...
					boolean hitAnyPlayer = false;
					for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
						if (nHitObject == gPlayer[p].nSprite) {
							// if ( dword_516B4 != 0 ) XXX
							// sub_32E4C(2000, 20, 30);

							// We have made a workaround so that player projectiles always pass through the player.
							assert !isPlayerProjectile(spr);
							assert isEnemyProjectile(spr);

							if (spr.picnum == REDDUDE_PROJ) {
								// Note: In the DOS version of L7P, when the projectile of the Red Dude hits
								// the player, the whole screen flashes white instead of red.  The commented
								// code seems to implement this by changing the global palette.
								//
								// changepalette(palette2); XXX
								// word_58784[target] = 1;
								// dword_569DC[target] = totalclock;

								playsound(7);
								gPlayer[p].nWeaponImpact = -72;
								gPlayer[p].nWeaponImpactAngle = (short) gPlayer[p].ang;
							} else {
								switch (spr.picnum) {
									case GREENDUDE_PROJ -> {
										sprite[blowspr].picnum = 1844;
										sprite[blowspr].shade = -18;
										sprite[blowspr].xrepeat = 24;
										sprite[blowspr].yrepeat = 24;
										sprite[blowspr].lotag = 64;
										playsound(33);
										gPlayer[p].nWeaponImpact = -100;
										gPlayer[p].nWeaponImpactAngle = (short) gPlayer[p].ang;
									}
									case PURPLEDUDE_PROJ -> {
										sprite[blowspr].picnum = 2356;
										sprite[blowspr].shade = -16;
										sprite[blowspr].xrepeat = 32;
										sprite[blowspr].yrepeat = 32;
										sprite[blowspr].lotag = 160;
										playsound(1);
										gPlayer[p].nWeaponImpact = -100;
										gPlayer[p].nWeaponImpactAngle = (short) gPlayer[p].ang;
									}
									case YELLOWDUDE_PROJ -> {
										sprite[blowspr].picnum = 2612;
										sprite[blowspr].shade = -16;
										sprite[blowspr].xrepeat = 32;
										sprite[blowspr].yrepeat = 32;
										sprite[blowspr].lotag = 160;
										playsound(5);
										gPlayer[p].nWeaponImpact = -100;
										gPlayer[p].nWeaponImpactAngle = (short) gPlayer[p].ang;
									}
									// Note: Do nothing for skull and buddha projectiles.
								}
								sprite[blowspr].z = spr.z + 2048;
							}

							assert spr.lotag < 0;
							changehealth(p, spr.lotag, ChangeHealthKind.HIT_DAMAGE);

							engine.deletesprite(i);
							hitAnyPlayer = true;
							break;
						}
					}
					if (hitAnyPlayer) {
						continue;
					}

					// If it hits a dude...
					if (isAnyDudeSprite(sprite[nHitObject].picnum)) {
						if (isEnemyProjectile(spr)) {
							// Enemy projectiles do not damage dudes directly, but their explosions do damage dudes.
							engine.deletesprite(i);
							continue;
						}

						int dude = canonicalDude(sprite[nHitObject].picnum);
						for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
							if ((spr.owner - 4096) == p) {
								if (sprite[nHitObject].lotag > 0) {
									sprite[nHitObject].lotag -= spr.lotag + gPlayer[p].nSecondWeaponDamage;
									gEnemyClock[nHitObject] = lockclock;
								}
							}
						}

						if (sprite[nHitObject].lotag > 0) {
							playEnemyHitSound(spr.picnum, dude, nHitObject);
							Enemies.enemyHit((short) nHitObject);

							// The following commented code looks like player can change the direction of the dude by attacking him.
//							if (sprite[nHitObject].lotag <= 12)
//								sprite[nHitObject].cstat &= 0xFD;
//							sprite[nHitObject].ang = (short) ((sprite[gPlayer[target].nSprite].ang + 0x400) & 0x7FF);
						} else {
							playEnemyDeathSound(spr.picnum, dude, nHitObject);
							enemydie(nHitObject);
						}

						if (pierce) {
							continue;
						}

						if (cfg.bRevisedQigong6 && spr.picnum == QIGONG6_PROJ) {
							if (spr.hitag >= 3) {
								spr.hitag -= 3;
								// Bounce from the enemy to a new target;
								qigong6FreeSearchNewTarget(spr, nHitObject);
								continue;
							}
						}

						engine.deletesprite(i);
						continue;
					}

					// When reached here, the projectile hits a sprite that is neither a player nor a dude.

					// Is it breakable?
					boolean breakable = isBreakable(sprite[nHitObject].picnum);
					if (breakable) {
						// Let's break the thing first if it is hit by a Qigong.
						if (isQigongProjectile(spr.picnum)) {
							playsound((engine.krand() & 7) + 37, i);
							breakBreakable(sprite[nHitObject]);
						}
					}

					if (cfg.bRevisedAnqi2 && spr.picnum == ANQI2_PROJ) {
						// In the revised game, bounce the metal ball off the sprite
						if (spr.hitag > 0) {
							spr.hitag -= 1;
							bounceFromSolidSprite(spr, sprite[nHitObject]);
							continue;
						}
					}

					if (!cfg.bRevisedQigong6 && spr.picnum == QIGONG6_PROJ) { // boomerang
						// In the traditional game, do nothing special.
						// If it hits a breakable, it's broken;
						// if not breakable, the projectile simply changes angle, and will eventually spin away.
						continue;
					}

					if (cfg.bRevisedQigong6 && spr.picnum == QIGONG6_PROJ) {
						// In the revised game, it behaves differently depending on whether the sprite is broken.
						if (spr.hitag > 0) {
							spr.hitag -= 1;
							if (breakable) {
								// Search a new target in any direction since the sprite has been broken.
								qigong6FreeSearchNewTarget(spr, nHitObject);
							} else {
								// Since the sprite is unbreakable, the projectile has to search targets in the right direction.
								// Otherwise, it will get stuck on the sprite.
								qigong6BounceFromSolidSprite(spr, sprite[nHitObject]);
							}
							continue;
						}
					}

					if (pierce) {
						// In the revised game, piercing projectiles go through all sprites.
						continue;
					}
				}

				// Finally delete the projectile.
				engine.deletesprite(i);
			}
		}

		for (Runnable job : postponedJobs) {
			job.run();
		}
		postponedJobs.clear();
	}

	public static void statuslistcode() {
		moveprojectiles();
		moveenemies();

		short i, nexti;
		i = headspritestat[SHOTSPARK];
		while (i >= 0) {
			nexti = nextspritestat[i];
			game.pInt.setsprinterpolate(i, sprite[i]);

			sprite[i].z -= TICSPERFRAME << 6;
			sprite[i].lotag -= TICSPERFRAME;
			if (sprite[i].lotag < 0)
				engine.deletesprite(i);

			i = nexti;
		}

		i = headspritestat[STATNUM_UNUSED4]; // unused?
		while (i >= 0) {
			nexti = nextspritestat[i];

			sprite[i].lotag -= TICSPERFRAME;
			sprite[i].picnum = (short) (((63 - sprite[i].lotag) >> 4) + 144);
			if (sprite[i].lotag < 0)
				engine.deletesprite(i);

			i = nexti;
		}

		i = headspritestat[EXPLOSION];
		while (i >= 0) {
			nexti = nextspritestat[i];

			short sectnum = sprite[i].sectnum;
			if (!cfg.bOriginal && sprite[i].picnum != 1364) {
				for (short spr = headspritesect[sectnum]; spr != -1; spr = nextspritesect[spr]) {
					switch (sprite[spr].picnum) {
					case PLAYER:
					case BLUEDUDE:
					case GREENDUDE:
					case REDDUDE:
					case PURPLEDUDE:
					case YELLOWDUDE:
						int dx = sprite[i].x - sprite[spr].x;
						int dy = sprite[i].y - sprite[spr].y;
						int dist = klabs(dx) + klabs(dy);
						if (dist <= sprite[i].xrepeat << 3) {

							if (engine.cansee(sprite[i].x, sprite[i].y, sprite[i].z, sectnum, sprite[spr].x,
									sprite[spr].y, sprite[spr].z - (engine.getTile(sprite[spr].picnum).getHeight() << 7),
									sprite[spr].sectnum)) {
								if (sprite[spr].picnum != PLAYER) {
									if (sprite[spr].statnum == GUARD)
										engine.changespritestat(spr, CHASE);
									sprite[spr].lotag--;
									if (sprite[spr].lotag <= 0) {
										if (sprite[spr].picnum == PURPLEDUDE || sprite[spr].picnum == BLUEDUDE)
											playsound(14, spr);
										else if (sprite[spr].picnum == YELLOWDUDE)
											playsound(18, spr);
										else
											playsound(17, spr);
										enemydie(spr);
									}
								} else {
									for (short p = connecthead; p >= 0; p = connectpoint2[p]) {
										if (spr == gPlayer[p].nSprite) {
											if(gPlayer[p].nPlayerStatus != 5) {
												// It used to directly remove health from player.
												// We use changehealth instead.
												changehealth(p, -1, ChangeHealthKind.EXPLOSION_DAMAGE);
												TintPalette(3, 0, 0);
											}
											gPlayer[p].nWeaponImpact = -32;
											gPlayer[p].nWeaponImpactAngle = engine.getangle(dx, dy);
										}
									}
								}
							}
						}
						break;
					}
				}
			}

			sprite[i].lotag -= TICSPERFRAME;
			if (sprite[i].lotag < 0)
				engine.deletesprite(i);

			i = nexti;
		}

		i = headspritestat[STATNUM_UNUSED7]; // unused?
		while (i >= 0) {
			nexti = nextspritestat[i];

			SPRITE spr = sprite[i];
			game.pInt.setsprinterpolate(i, spr);

			spr.x += TICSPERFRAME * spr.xvel >> 4;
			spr.y += TICSPERFRAME * spr.yvel >> 4;
			spr.z += TICSPERFRAME * spr.zvel >> 4;
			spr.zvel += TICSPERFRAME << 7;

			if ((sector[spr.sectnum].ceilingz + 1024) > spr.z) {
				spr.z = sector[spr.sectnum].ceilingz + 1024;
				spr.zvel = (short) -(spr.zvel >> 1);
			}

			if (sector[spr.sectnum].floorz - 1024 < spr.z) {
				spr.z = sector[spr.sectnum].floorz - 1024;
				spr.zvel = (short) -(spr.zvel >> 1);
			}

			spr.xrepeat = (short) (spr.lotag >> 2);
			spr.yrepeat = (short) (spr.lotag >> 2);
			spr.lotag -= TICSPERFRAME;
			if (spr.lotag < 0)
				engine.deletesprite(i);

			i = nexti;
		}
	}

	/**
	 * Result of reflection computing.
	 *
	 * @param nx The X component of the normal vector, pointing out from the wall.
	 * @param ny The Y component of the normal vector, pointing out from the wall.
	 */
	record ReflectionInfo(double nx,
						  double ny) {}

	/**
	 * Make a projectile bounce from the surface of a wall or a sprite, respecting the law of reflection.
	 * This method will alter the X and Y velocity and the angle.
	 * <p>
	 * nx and ny is a normal vector of the reflection surface, i.e. any vector that is perpendicular to the wall.
	 *
	 * @param spr The projectile.
	 * @param nx The X component of the normal vector.
	 * @param ny The Y component of the normal vector.
	 * @return Information about this reflection.
	 */
	private static ReflectionInfo bounceFromSolidSurface(SPRITE spr, double nx, double ny) {
		double sprXVel = spr.xvel;
		double sprYVel = spr.yvel;
		double projFactor = (sprXVel * nx + sprYVel * ny) / (nx * nx + ny * ny);
		double projX = projFactor * nx;
		double projY = projFactor * ny;
		double dxVel = - 2 * projX;
		double dyVel = - 2 * projY;
		double newSprXVel = sprXVel + dxVel;
		double newSprYVel = sprYVel + dyVel;
		spr.xvel = (short) newSprXVel;
		spr.yvel = (short) newSprYVel;
		spr.ang = engine.getangle(spr.xvel, spr.yvel);
		return new ReflectionInfo(dxVel, dyVel);
	}

	/**
	 * Make a projectile bounce from a wall.
	 *
	 * @param spr The projectile.
	 * @param hitIndex The index of the wall it hits.
	 * @return Information about this reflection.
	 */
	private static ReflectionInfo bounceFromWall(SPRITE spr, int hitIndex) {
		double wallDx = wall[wall[hitIndex].point2].x - wall[hitIndex].x;
		double wallDy = wall[wall[hitIndex].point2].y - wall[hitIndex].y;
		return bounceFromSolidSurface(spr, wallDy, -wallDx);
	}

	/**
	 * Make a projectile bounce from another sprite as if the other sprite were a solid cylinder.
	 *
	 * @param spr The projectile.
	 * @param hit The sprite which spr hits.
	 * @return Information about this reflection.
	 */
	private static ReflectionInfo bounceFromSolidSprite(SPRITE spr, SPRITE hit) {
		double nx = spr.x - hit.x;
		double ny = spr.y - hit.y;
		return bounceFromSolidSurface(spr, nx, ny);
	}

	/**
	 * Make a projectile of Qigong #6 bounce from a solid surface.
	 * It will search for another enemy and, if not found, just reflect.
	 *
	 * @param spr The projectile.
	 * @param nx The X component of the normal vector.
	 * @param ny The Y component of the normal vector.
	 */
	private static void qigong6BounceFromSolidSurface(SPRITE spr, double nx, double ny) {
		ReflectionInfo refInfo = bounceFromSolidSurface(spr, nx, ny);
		qigong6RetargetAfterReflection(spr, refInfo);
	}

	/**
	 * Make a projectile of Qigong #6 bounce from a wall.
	 * It will search for another enemy and, if not found, just reflect.
	 *
	 * @param spr The projectile.
	 * @param hitIndex The index of the wall it hits.
	 */
	private static void qigong6BounceFromWall(SPRITE spr, int hitIndex) {
		ReflectionInfo refInfo = bounceFromWall(spr, hitIndex);
		qigong6RetargetAfterReflection(spr, refInfo);
	}

	/**
	 * Make a projectile of Qigong #6 bounce from another sprite.
	 * It will search for another enemy and, if not found, just reflect.
	 *
	 * @param spr The projectile.
	 * @param hit The sprite which spr hits.
	 */
	private static void qigong6BounceFromSolidSprite(SPRITE spr, SPRITE hit) {
		ReflectionInfo refInfo = bounceFromSolidSprite(spr, hit);
		qigong6RetargetAfterReflection(spr, refInfo);
	}

	/**
	 * This method is a wrapper of {@link ru.m210projects.LSP.Factory.LSPEngine#movesprite} that emulates piercing.
	 */
	private static short pierceMoveSprite(short spritenum, int dx, int dy, int dz, int clipdist, int ceildist, int flordist,
							int cliptype, int vel) {
		SPRITE spr = sprite[spritenum];

		// Save the old coordinate.
		int oldX = spr.x;
		int oldY = spr.y;
		int oldZ = spr.z;

		// Ignore all sprites and try moving.
		engine.movesprite(spritenum, dx, dy, dz, clipdist, ceildist, flordist, cliptype & 0xFFFF, vel);

		// Save where this project has been when ignoring sprites.
		int pierceX = spr.x;
		int pierceY = spr.y;
		int pierceZ = spr.z;

		// Restore the old coordinate.
		spr.x = oldX;
		spr.y = oldY;
		spr.z = oldZ;

		// Try moving again. This time, we don't ignore sprites, so it can hit something.  We record what it hits.
		short hitmove = engine.movesprite(spritenum, dx, dy, dz, clipdist, ceildist, flordist, cliptype, vel);

		// ... but we manually move the projectile to the location where it moved when ignoring sprites.
		spr.x = pierceX;
		spr.y = pierceY;
		spr.z = pierceZ;

		return hitmove;
	}

	record AutoTargetInfo(int target,
						  int dx,
						  int dy,
						  int dz) {
	}

	public static int getDudeCenterHeight(SPRITE spr) {
		// Why should it be (height * yrepeat)? Shouldn't it be ((height * yrepeat) / 2)?
		return spr.z - (engine.getTile(spr.picnum).getHeight() * spr.yrepeat);
	}

	enum TargetingMode {
		ANGLE, DISTANCE,
	}

	public static Optional<AutoTargetInfo> qigong6AutoTarget(int x, int y, int z, int sect, TargetingMode mode, double vx, double vy, double minCos, int excludeDude) {
		int nearest = -1;
		double minKey = Double.POSITIVE_INFINITY;
		for (int state : TARGETABLE_STATES) {
			for (short i = headspritestat[state]; i != -1; i = nextspritestat[i]) {
				if (i == excludeDude) {
					continue;
				}

				SPRITE spr = sprite[i];
				if (!isAnyDudeSprite(spr.picnum)) {
					continue;
				}

				int sx = spr.x;
				int sy = spr.y;
				int dx = sx - x;
				int dy = sy - y;
				double dot2D = vx * dx + vy * dy;
				double cos2D = dot2D / (Math.hypot(dx, dy) * Math.hypot(vx, vy));
				if (cos2D < minCos) {
					continue;
				}

				int sz = getDudeCenterHeight(spr);
				if (!engine.cansee(x, y, z, sect, sx, sy, sz, spr.sectnum)) {
					continue;
				}

				double key = switch (mode) {
					case ANGLE -> -cos2D;
					case DISTANCE -> Math.hypot(dx, dy);
				};

				if (key < minKey) {
					minKey = key;
					nearest = i;
				}
			}
		}

		if (nearest != -1) {
			SPRITE spr = sprite[nearest];
			int sx = spr.x;
			int sy = spr.y;
			int sz = getDudeCenterHeight(spr);
			return Optional.of(new AutoTargetInfo(nearest, sx - x, sy - y, sz - z));
		} else {
			return Optional.empty();
		}
	}

	/**
	 * Make a projectile of Qigong #6 search for a new target in any direction.
	 * <p>
	 * It will ignore the sprite {@code excludeDude} when searching.
	 * When the projectile hits a dude, it shall ignore that dude and search for another enemy.
	 * But if the new target behind the old target, it will just hit the old target again (poor guy :P).
	 *
	 * @param spr The projectile.
	 * @param excludeDude The dude to ignore.
	 */
	private static void qigong6FreeSearchNewTarget(SPRITE spr, int excludeDude) {
		qigong6AutoTarget(
				spr.x, spr.y, spr.z, spr.sectnum,
				TargetingMode.DISTANCE, spr.xvel, spr.yvel, -1,
				excludeDude).ifPresent((ti) -> {
			// If a new target is found, go for it.
			setQigong6Velocity(spr, ti.dx(), ti.dy(), ti.dz());
		});
	}

	/**
	 * Make a projectile of Qigong #6 search for a new target in the direction of reflection.
	 * <p>
	 * It will only search a 180-degree range away from the reflected surface.
	 *
	 * @param spr The projectile.
	 * @param refInfo The information of the reflection
	 */
	private static void qigong6RetargetAfterReflection(SPRITE spr, ReflectionInfo refInfo) {
		qigong6AutoTarget(
				spr.x, spr.y, spr.z, spr.sectnum,
				TargetingMode.DISTANCE, refInfo.nx, refInfo.ny, 0,
				-1).ifPresent((ti) -> {
			// If a new target is found, go for it.
			setQigong6Velocity(spr, ti.dx(), ti.dy(), ti.dz());
		});
	}

	public static void setQigong6Velocity(SPRITE spr, int dx, int dy, int dz) {
		double hDist = Math.hypot(dx, dy);
		double hVel = 512; // from the `>> 5` below
		double zVel = hVel * dz / hDist;
		spr.ang = engine.getangle(dx, dy);
		spr.xvel = (short) (sintable[(spr.ang + 2560) & 0x7FF] >> 5);
		spr.yvel = (short) (sintable[(spr.ang + 2048) & 0x7FF] >> 5);
		spr.zvel = (short) zVel;
	}
}
